package com.example.smart.catchngo.BD_Pokemon;

import java.util.List;

import io.reactivex.Flowable;

public class PokemonDataSource implements IPokemonDataSource {

    private PokemonDAO pokemonDAO;
    private static PokemonDataSource mInstance;


    public PokemonDataSource(PokemonDAO pokemonDAO) {
        this.pokemonDAO = pokemonDAO;
    }

    public static PokemonDataSource getInstance(PokemonDAO pokemonDAO) {
        if (mInstance == null) {
            mInstance = new PokemonDataSource(pokemonDAO);
        }
        return mInstance;
    }

    @Override
    public Flowable<Pokemon> getPokemonById(int pokemonId) {
        return pokemonDAO.getPokemonById(pokemonId);
    }

    @Override
    public Flowable<List<Pokemon>> getAllPokemons() {
        return pokemonDAO.getAllPokemons();
    }

    @Override
    public Pokemon getPokemonTrainer(int i) {
        return pokemonDAO.getPokemonTrainer(i);
    }

    @Override
    public void insertPokemon(Pokemon... pokemons) {
        pokemonDAO.insertPokemon(pokemons);
    }

    @Override
    public void updatePokemon(Pokemon... pokemons) {
        pokemonDAO.updatePokemon(pokemons);
    }

    @Override
    public void deletePokemon(Pokemon pokemon) {
        pokemonDAO.deletePokemon(pokemon);
    }

    @Override
    public void deleteAllPokemons() {
        pokemonDAO.deleteAllPokemons();
    }
}
