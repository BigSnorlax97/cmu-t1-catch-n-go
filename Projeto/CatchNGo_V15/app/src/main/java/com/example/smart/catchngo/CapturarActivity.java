package com.example.smart.catchngo;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.smart.catchngo.BD.User;
import com.example.smart.catchngo.BD.UserDataSource;
import com.example.smart.catchngo.BD.UserDatabase;
import com.example.smart.catchngo.BD.UserRepository;
import com.example.smart.catchngo.BD_Pokemon.Pokemon;
import com.example.smart.catchngo.BD_Pokemon.PokemonDataSource;
import com.example.smart.catchngo.BD_Pokemon.PokemonDatabase;
import com.example.smart.catchngo.BD_Pokemon.PokemonRepository;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class CapturarActivity extends AppCompatActivity implements View.OnClickListener {

    private Button button_capturar_pokemon;
    private ImageView imageView_pokemon, imageView_change_ball;
    private TextView textView_pokemon_nome, textView_pokemon_nivel, textView_location, textView_sucesso_captura;
    private int ball = 1, bcr = -1, exp_base = 0;
    private String nome = "";
    private Pokemon pokemon = new Pokemon();

    //Location
    private LocationManager locationManager;
    private LocationListener locationListener;
    private String locationProvider = LocationManager.NETWORK_PROVIDER;

    //Adapter
    private List<Pokemon> pokemonList = new ArrayList<>();
    private ArrayAdapter adapter;

    //Database
    private CompositeDisposable compositeDisposable;
    private PokemonRepository pokemonRepository;

    private Random rnd = new Random();
    private boolean gotFirstLocation = false;

    //Adapter
    private List<User> userList = new ArrayList<>();

    //Database
    private CompositeDisposable userCompositeDisposable;
    private UserRepository userRepository;

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capturar);

        button_capturar_pokemon = findViewById(R.id.button_capturar_pokemon);
        imageView_pokemon = findViewById(R.id.imageView_pokemon);
        textView_pokemon_nome = findViewById(R.id.textView_pokemon_nome);
        textView_pokemon_nivel = findViewById(R.id.textView_pokemon_nivel);
        imageView_change_ball = findViewById(R.id.imageView_change_ball);
        textView_location = findViewById(R.id.textView_location);
        textView_sucesso_captura = findViewById(R.id.textView_sucesso_captura);

        //criar o pokemon a capturar
        Random rnd = new Random();
        int num = rnd.nextInt(802) + 1;
        int nivel = rnd.nextInt(95) + 5;

        pokemon.setNivel(nivel); //aleatorio
        pokemon.setNumero(num); //aleatorio
        pokemon.setImage_url();
        pokemon.setTreinador(LoginActivity.EMAIL); //guarda o email do utilizador no pokemon

        String url = "https://pokeapi.co/api/v2/pokemon/" + num + "/"; //url para usar a PokeAPI, para obter o nome e o base_experience
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    nome = response.getString("name"); //nome do Pokemon
                    pokemon.setName(nome.substring(0, 1).toUpperCase() + nome.substring(1)); //colocar a primeira letra do nome maiuscula
                    exp_base = Integer.parseInt(response.getString("base_experience")); //experiencia que esse Pokemon dá ao utilizador
                    mostrarDadosPokemon(); //mostra o nome, nivel e imagem do Pokemon
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jor);

        Picasso.get().load(pokemon.getImage_url()).into(imageView_pokemon); //vai buscar a imagem do Pokemon ao url da imagem (que é igual a todos, só muda o numero)

        String url_2 = "https://pokeapi.co/api/v2/pokemon-species/" + num + "/"; //url para usar a PokeAPI, para obter o capture rate
        JsonObjectRequest jor_2 = new JsonObjectRequest(Request.Method.GET, url_2, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    bcr = Integer.parseInt(response.getString("capture_rate")); //capture rate (varia entre 3 e 255), que define a dificuldade de captura
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
            }
        });
        queue.add(jor_2);

        //Init
        compositeDisposable = new CompositeDisposable();

        //Database
        PokemonDatabase pokemonDatabase = PokemonDatabase.getInstance(this); //create database
        pokemonRepository = PokemonRepository.getInstance(PokemonDataSource.getInstance(pokemonDatabase.pokemonDAO()));

        //Load all data from Database
        loadData();

        button_capturar_pokemon.setEnabled(false);
        button_capturar_pokemon.setText("Wait Loading...");

        location();

        //Init
        userCompositeDisposable = new CompositeDisposable();

        //Database
        UserDatabase userDatabase = UserDatabase.getInstance(this); //create database
        userRepository = UserRepository.getInstance(UserDataSource.getInstance(userDatabase.userDAO()));

        loadDataUser();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                findUser();

                Runnable runnable = new CapturarActivity.CountDownRunner();
                Thread myThread = new Thread(runnable);
                myThread.start();

                button_capturar_pokemon.setOnClickListener(CapturarActivity.this);
                imageView_change_ball.setOnClickListener(CapturarActivity.this);

            }
        };
        Handler h = new Handler();
        h.postDelayed(r, 1000);
    }

    class CountDownRunner implements Runnable {
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    doWork();
                    Thread.sleep(10); //corre a cada 10 milesimos de segundo
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                } catch (Exception e) {
                }
            }
        }
    }

    /**
     * Verifica se o conseguiu obter o nome do Pokemon, a localizaçap
     * Caso nao tenha o nome ou a localizaçao, o utilizador nao consegue capturar o Pokemon
     * Caso o utilizador nao tenha um tipo de pokebolas, não lhe é permitido usar esse tipo de pokebolas
     * Caso nao tenha Pokebolas, o utilizador é informado que precisa de ir receber mais itens e nao lhe é permitido capturar o Pokemon
     */
    public void doWork() {
        runOnUiThread(new Runnable() {
            public void run() {
                try {
                    int[] bolas = getPokebolas();
                    if (!gotFirstLocation) {
                        if (textView_pokemon_nome.getText().toString().isEmpty() || textView_location.getText().toString().equals("Procurando localização...")) {
                            button_capturar_pokemon.setEnabled(false);
                        } else {
                            if (bolas[0] <= 0 && bolas[1] <= 0 && bolas[2] <= 0 && bolas[3] <= 0) {
                                button_capturar_pokemon.setEnabled(false);
                                button_capturar_pokemon.setText("Sem Pokebolas (para receber mais vá aos Itens)");
                                imageView_change_ball.setImageDrawable(getDrawable(R.drawable.pokeball));
                                imageView_change_ball.setEnabled(false);
                                gotFirstLocation = true;
                            } else {
                                button_capturar_pokemon.setEnabled(true);
                                button_capturar_pokemon.setText("Capturar");
                                gotFirstLocation = true;
                            }
                        }
                    }
                    if (bolas[0] <= 0 && bolas[1] <= 0 && bolas[2] <= 0 && bolas[3] <= 0) {

                    } else if (ball == 1 && bolas[0] <= 0) {
                        imageView_change_ball.setImageDrawable(getDrawable(R.drawable.greatball));
                        ball = 2;
                    } else if (ball == 2 && bolas[1] <= 0) {
                        imageView_change_ball.setImageDrawable(getDrawable(R.drawable.ultraball));
                        ball = 3;
                    } else if (ball == 3 && bolas[2] <= 0) {
                        imageView_change_ball.setImageDrawable(getDrawable(R.drawable.masterball));
                        ball = 4;
                    } else if (ball == 4 && bolas[3] <= 0) {
                        imageView_change_ball.setImageDrawable(getDrawable(R.drawable.pokeball));
                        ball = 1;
                    }
                } catch (Exception e) {
                }
            }
        });
    }

    /**
     * Carrega os dados dos utilizadores da base de dados
     */
    private void loadDataUser() {
        Disposable disposable = userRepository.getAllUsers()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<User>>() {
                    @Override
                    public void accept(List<User> users) throws Exception {
                        onGetAllUserSuccess(users);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(CapturarActivity.this, "fail\n" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        userCompositeDisposable.add(disposable);
    }

    /**
     * Guarda todos os utilizadores numa lista
     *
     * @param users - lista de utilizadores
     */
    private void onGetAllUserSuccess(List<User> users) {
        userList.clear();
        userList.addAll(users);
    }

    /**
     * Procura o utilizador na base de dados
     */
    public void findUser() {
        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).getEmail().equals(LoginActivity.EMAIL)) {
                user = userList.get(i);
            }
        }
    }

    /**
     * Atualiza os dados do utilizador
     *
     * @param user - objeto do tipo User
     */
    public void updateUser(final User user) {
        Disposable disposable = Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> e) throws Exception {
                userRepository.updateUser(user);
                e.onComplete();
            }
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object o) throws Exception {
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(CapturarActivity.this, "2" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, new Action() {
                    @Override
                    public void run() throws Exception {
                        loadData(); //Refresh data
                    }
                });
        userCompositeDisposable.add(disposable);
    }

    /**
     * Carrega os dados dos Pokemon da base de dados
     */
    private void loadData() {
        Disposable disposable = pokemonRepository.getAllPokemons()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<Pokemon>>() {
                    @Override
                    public void accept(List<Pokemon> pokemons) throws Exception {
                        OnGetAllPokemonSuccess(pokemons);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //Toast.makeText(CapturarActivity.this, "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        compositeDisposable.add(disposable);
    }

    /**
     * Guarda os Pokemons numa lista
     *
     * @param pokemons - lista de Pokemon
     */
    private void OnGetAllPokemonSuccess(List<Pokemon> pokemons) {
        pokemonList.clear();
        pokemonList.addAll(pokemons);
        adapter.notifyDataSetChanged();
    }

    /**
     * Mostra o nome e o nivel do Pokemon
     */
    public void mostrarDadosPokemon() {
        textView_pokemon_nome.setText(pokemon.getName());
        textView_pokemon_nivel.setText("Nivel: " + String.valueOf(pokemon.getNivel()));
    }

    /**
     * Ao clicar na imagem da pokebola, muda para o proximo tipo de pokebola
     */
    public void changePokeball() {
        if (ball == 1) {
            imageView_change_ball.setImageDrawable(getDrawable(R.drawable.greatball));
            ball = 2;
        } else if (ball == 2) {
            imageView_change_ball.setImageDrawable(getDrawable(R.drawable.ultraball));
            ball = 3;
        } else if (ball == 3) {
            imageView_change_ball.setImageDrawable(getDrawable(R.drawable.masterball));
            ball = 4;
        } else if (ball == 4) {
            imageView_change_ball.setImageDrawable(getDrawable(R.drawable.pokeball));
            ball = 1;
        } else {
            imageView_change_ball.setImageDrawable(getDrawable(R.drawable.zero));
            ball = 1;
        }
    }

    /**
     * Obtem a localizaçao do utlizador (latitude, longitude, cidade e Pais)
     */
    @TargetApi(Build.VERSION_CODES.M)
    public void location() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                pokemon.setLatitude(String.valueOf(location.getLatitude())); //latitude
                pokemon.setLongitude(String.valueOf(location.getLongitude())); //longitude
                textView_location.setText("Lat: " + pokemon.getLatitude() + "\nLon: " + pokemon.getLongitude()); //mostra a latitude e longitude
                String local = hereLocation(location.getLatitude(), location.getLongitude()); //vai buscar a cidade e Pais onde o utilizador se encontra
                textView_location.append("\n" + local); //mostra o a cidade e Pais
                pokemon.setLocalizacao(local); //adiciona a cidade e Pais ao Pokemon
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        };
        //Permissoes de localizaçao
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET
                }, 10);
            } else {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET
                }, 10);
            }
            return;

        } else {
            configureButton();
        }
        configureButton();
    }

    /**
     * Devolve a cidade e pais, tendo em conta o latitude e longitude
     *
     * @param lat - latitude
     * @param lon - longitude
     * @return string com a cidade e Pais
     */
    public String hereLocation(double lat, double lon) {
        String cityName = "";

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(lat, lon, 10);
            if (addresses.size() > 0) {
                for (Address adr : addresses) {
                    if (adr.getLocality() != null && adr.getLocality().length() > 0) {
                        cityName = adr.getLocality() + ", " + adr.getCountryName();
                        break;
                    }
                }
            }
        } catch (IOException e) {
        }
        return cityName;
    }

    //Permissoes
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 10:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    configureButton();
                return;
        }
    }

    private void configureButton() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        locationManager.requestLocationUpdates(locationProvider, 0, 0, locationListener);
    }

    /**
     * Se o utilizador usar a Masterball (ball = 4) a captura devolve sempre como sucesso
     * Se usar uma das outras pokebolas o utilizador tem diferentes probabilidade de sucesso
     *
     * @return sucesso ou insucesso da captura
     */
    private boolean capturarPokemon() {
        capturandoWait(); //espera... para causar suspense durante a captura
        if (ball == 4) {
            return true;
        } else {
            int number = 0;
            if (ball == 1) {
                number = (rnd.nextInt(200 + pokemon.getNivel()));
            } else if (ball == 2) {
                number = rnd.nextInt(150 + pokemon.getNivel());
            } else if (ball == 3) {
                number = rnd.nextInt(100 + pokemon.getNivel());
            } else {
                Toast.makeText(this, "Pokebola Inválida....", Toast.LENGTH_SHORT).show();
            }
            if (number <= bcr + 10) {
                return true;
            }
        }
        return false;

    }

    /**
     * Server apenas para a captura nao ser tão imediata
     */
    private void capturandoWait() {
        imageView_change_ball.setEnabled(false);
        button_capturar_pokemon.setEnabled(false);
        button_capturar_pokemon.setText("Capturando");
        textView_sucesso_captura.setText("");

        Runnable r = new Runnable() {
            @Override
            public void run() {
                button_capturar_pokemon.setText("Capturando.");
            }
        };
        Handler h = new Handler();
        h.postDelayed(r, 1000);

        r = new Runnable() {
            @Override
            public void run() {
                button_capturar_pokemon.setText("Capturando..");
            }
        };
        h = new Handler();
        h.postDelayed(r, 2000);

        r = new Runnable() {
            @Override
            public void run() {
                button_capturar_pokemon.setText("Capturando...");
            }
        };
        h = new Handler();
        h.postDelayed(r, 3000);
    }

    /**
     * Lê os itens do utilizador que estão guardados no formato de String e converte para um array de inteiros
     *
     * @return array de inteiros com a quantidade de cada item
     */
    public int[] getPokebolas() {
        String itens = user.getItens();
        String[] array = itens.split(":");
        int[] bolas = new int[4];
        bolas[0] = Integer.parseInt(array[0]);
        bolas[1] = Integer.parseInt(array[1]);
        bolas[2] = Integer.parseInt(array[2]);
        bolas[3] = Integer.parseInt(array[3]);
        return bolas;
    }

    /**
     * Diminui a quantide de pokebolas quando estão são utilizadas para capturar um Pokemon
     */
    public void usarPokebola() {
        String itens;
        int array[] = getPokebolas();
        if (ball == 1) {
            array[0] = array[0] - 1;
        }
        if (ball == 2) {
            array[1] = array[1] - 1;
        }
        if (ball == 3) {
            array[2] = array[2] - 1;
        }
        if (ball == 4) {
            array[3] = array[3] - 1;
        }
        itens = array[0] + ":" + array[1] + ":" + array[2] + ":" + array[3];
        user.setItens(itens);
        updateUser(user);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_capturar_pokemon) {
            usarPokebola();
            if (capturarPokemon()) {
                pokemon.setData_captura(Calendar.getInstance().getTimeInMillis());
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        button_capturar_pokemon.setText("CAPTURADO!!!"); //botao mostra que o Pokemon foi capturado
                        textView_sucesso_captura.setText("CAPTURADO!!!"); //textView mostra que o Pokemon foi capturado
                    }
                };
                Handler h = new Handler();
                h.postDelayed(r, 4000);
                r = new Runnable() {
                    @Override
                    public void run() {
                        Disposable disposable = Observable.create(new ObservableOnSubscribe<Object>() {
                            @Override
                            public void subscribe(ObservableEmitter<Object> e) throws Exception {
                                pokemonRepository.insertPokemon(pokemon);
                                e.onComplete();
                            }
                        })
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.io())
                                .subscribe(new Consumer<Object>() {
                                    @Override
                                    public void accept(Object o) throws Exception {
                                        //Toast.makeText(CapturarActivity.this, "Pokemon added!!!", Toast.LENGTH_SHORT).show();
                                    }
                                }, new Consumer<Throwable>() {
                                    @Override
                                    public void accept(Throwable throwable) throws Exception {
                                        Toast.makeText(CapturarActivity.this, "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }, new Action() {
                                    @Override
                                    public void run() throws Exception {
                                        loadData(); //Refresh data
                                    }
                                });
                        startActivity(new Intent(CapturarActivity.this, MenuActivity.class));
                    }
                };
                h = new Handler();
                h.postDelayed(r, 5000);
                user.setExperiencia(user.getExperiencia() + 100 + pokemon.getNivel() + exp_base);
                updateUser(user);
            } else {
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        button_capturar_pokemon.setText("Captura falhada"); //botao mostra que o Pokemon nao foi capturado
                        textView_sucesso_captura.setText("Captura falhada"); //textView mostra que o Pokemon nao foi capturado
                    }
                };
                Handler h = new Handler();
                h.postDelayed(r, 4000);
                r = new Runnable() {
                    @Override
                    public void run() {
                        imageView_change_ball.setEnabled(true); //permite mudar de pokebola
                        button_capturar_pokemon.setEnabled(true); //permite tentar capturar o Pokemon
                        button_capturar_pokemon.setText("Capturar");
                    }
                };
                h = new Handler();
                h.postDelayed(r, 5000);
            }
        }
        if (v.getId() == R.id.imageView_change_ball) {
            changePokeball(); //muda o tipo de pokebola
        }
    }
}