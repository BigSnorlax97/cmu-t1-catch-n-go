package com.example.smart.catchngo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class PokemonIndividualActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView textView_pokemon_numero_nome, textView_descricao;
    private ImageView imageView_pokemon_image, imageView_anterior, imageView_atual, imageView_seguinte;
    private String nome, image_url, peso;
    private int numero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_individual);

        textView_pokemon_numero_nome = findViewById(R.id.textView_pokemon_numero_nome);
        imageView_pokemon_image = findViewById(R.id.imageView_pokemon_image);
        textView_descricao = findViewById(R.id.textView_pokemon_descricao);
        imageView_anterior = findViewById(R.id.imageView_anterior);
        imageView_atual = findViewById(R.id.imageView_atual);
        imageView_seguinte = findViewById(R.id.imageView_seguinte);

        textView_descricao.setVisibility(View.GONE); //na chegamos a mostrar a descrição visto que nao encontramos na API

        numero = Integer.parseInt(PokedexActivity.NUM);

        getBeforeAfterPokemon();
        getPokemonDex();

        imageView_anterior.setOnClickListener(this);
        imageView_seguinte.setOnClickListener(this);
    }

    /**
     * mostra as imagens/sprites do Pokemon anterior, do Pokemon atual e do Pokemon seguinte
     */
    public void getBeforeAfterPokemon() {
        String num_anterior = String.valueOf(numero-1);
        String num_atual = String.valueOf(numero);
        String num_seguinte = String.valueOf(numero+1);
        if(numero < 100){
            num_anterior = "0" + (numero-1);
            num_atual = "0" + numero;
            num_seguinte = "0" + (numero+1);
        }
        if(numero < 10){
            num_anterior = "00" + (numero-1);
            num_atual = "00" + numero;
            num_seguinte = "00" + (numero+1);
        }

        //se o Pokemon atual for o primeiro, quer dizer que nao tem um Pokemon anterior, por isso, mostra uma imagem "?" que simboliza que nao há pokemon anterior
        if (numero > 1) {
            image_url = "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/" + num_anterior + ".png";
            Picasso.get().load(image_url).into(imageView_anterior);
        } else {
            imageView_pokemon_image.setImageDrawable(getDrawable(R.drawable.zero));
        }

        //mostra a imagem/sprite do Pokemon atual
        image_url = "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/" + num_atual + ".png";
        Picasso.get().load(image_url).into(imageView_atual);

        //mostra o Pokemon seguinte, no momento existem 808 Pokemons, mas apenas 802 estão definidos na API usada, por isso, apesar de mostrar a imagem do Pokemon numero 803, não vamos poder mostrar os dados deste pokemon
        if (numero <= 802) {
            image_url = "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/" + num_seguinte + ".png";
            Picasso.get().load(image_url).into(imageView_seguinte);
        } else {
            imageView_pokemon_image.setImageDrawable(getDrawable(R.drawable.zero));
        }
    }

    /**
     * Sabendo o numero do Pokemon atual, mostra a imagem/sprite do Pokemon e faz request à API para decobrir o nome
     */
    public void getPokemonDex() {
        String num_atual = String.valueOf(numero);

        if(numero < 100){
            num_atual = "0" + numero;
        }
        if(numero < 10){
            num_atual = "00" + numero;
        }
        image_url = "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/" + num_atual + ".png";
        Picasso.get().load(image_url).into(imageView_pokemon_image);

        String url = "https://pokeapi.co/api/v2/pokemon/" + numero + "/";

        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    nome = response.getString("name");
                    if (numero < 10)
                        textView_pokemon_numero_nome.setText("No. 00" + numero);
                    else if (numero < 100)
                        textView_pokemon_numero_nome.setText("No. 0" + numero);
                    else
                        textView_pokemon_numero_nome.setText("No. " + numero);
                    textView_pokemon_numero_nome.append("   " + nome.substring(0, 1).toUpperCase() + nome.substring(1));

                    peso = response.getString("weight");
                    textView_descricao.setText("Peso: " + peso);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jor);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imageView_anterior) {
            if (numero > 1) {
                numero--;
                textView_pokemon_numero_nome.setText("");
                getPokemonDex();
                getBeforeAfterPokemon();
            }
        }
        if (v.getId() == R.id.imageView_seguinte) {
            if (numero < 802) {
                numero++;
                textView_pokemon_numero_nome.setText("");
                getPokemonDex();
                getBeforeAfterPokemon();
            }
        }
    }
}
