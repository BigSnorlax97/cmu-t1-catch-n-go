package com.example.smart.catchngo.BD_Pokemon;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Calendar;

@Entity(tableName = "pokemons")
public class Pokemon {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id; //id é unico para cada Pokemon. Sempre que um novo Pokemon é criado é atribuido um id diferente

    @ColumnInfo(name = "name")
    private String name; //nome do Pokemon. Nome especifico do Pokemon

    @ColumnInfo(name = "numero")
    private int numero; //numero do Pokemon. Numero especifico do Pokemon (entre 1 e 802)

    @ColumnInfo(name = "nivel")
    private int nivel; //nivel do Pokemon (entre 1 e 100)

    @ColumnInfo(name = "latitude")
    private String latitude; //Latitude do local de captura do Pokemon

    @ColumnInfo(name = "longitude")
    private String longitude; //Longitude do local de captura do Pokemon

    @ColumnInfo(name = "image")
    private String image_url; //imagem (sprite) do Pokemon. url da imagem

    @ColumnInfo(name = "localizacao")
    private String localizacao; //cidade e pais onde o Pokemon foi capturado

    @ColumnInfo(name = "data_captura")
    private long data_captura; //data de captura do Pokemon. tempo em milissegundos

    @ColumnInfo(name = "treinador")
    private String treinador; //email do treinador que capturou o Pokemon

    public Pokemon() {
    }

    @Ignore
    public Pokemon(String name, int numero, int nivel) {
        this.name = name;
        this.numero = numero;
        this.nivel = nivel;
        this.image_url = "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/" + numero + ".png";
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getImage_url() {

        String numero_pokemon = String.valueOf(numero);
        if (numero < 100)
            numero_pokemon = "0" + numero;
        if (numero < 10)
            numero_pokemon = "00" + numero;
        return "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/" + numero_pokemon + ".png";
    }

    public void setImage_url() {
        String numero_pokemon = String.valueOf(numero);
        if (numero < 100)
            numero_pokemon = "0" + numero;
        if (numero < 10)
            numero_pokemon = "00" + numero;
        image_url = "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/" + numero_pokemon + ".png";
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public long getData_captura() {
        return data_captura;
    }

    public void setData_captura(long data_captura) {
        this.data_captura = data_captura;
    }

    /**
     * Converte a data de captura do pokemon num string no formato DD/MM/AAAA
     *
     * @return string da data de captura
     */
    private String getDataCapturaString() {
        final Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(data_captura);

        final int dia = cal.get(Calendar.DAY_OF_MONTH);
        final int mes = cal.get(Calendar.MONTH) + 1;
        final int ano = cal.get(Calendar.YEAR);
        final int hours = cal.get(Calendar.HOUR_OF_DAY);
        final int minutes = cal.get(Calendar.MINUTE);
        final int seconds = cal.get(Calendar.SECOND);

        return (dia + "/" + mes + "/" + ano + "   " + hours + ":" + minutes + ":" + seconds);
    }

    public String getTreinador() {
        return treinador;
    }

    public void setTreinador(String treinador) {
        this.treinador = treinador;
    }

    @Override
    public String toString() {
        return new StringBuilder("\nTreinador: " + treinador + "\nid: " + id + "\nNome: " + name + "\nNº: " + numero + "\nNivel: " + nivel + "\nLatitude: " + latitude +
                "\nLongitude: " + longitude + "\n" + localizacao + "\n" + getDataCapturaString()).toString();
    }
}
