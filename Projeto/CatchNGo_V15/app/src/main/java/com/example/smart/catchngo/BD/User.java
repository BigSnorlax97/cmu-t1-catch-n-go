package com.example.smart.catchngo.BD;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Calendar;

@Entity(tableName = "users")
public class User {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id; //id é unico para cada user. Sempre que um novo user é criado é atribuido um id diferente

    @ColumnInfo(name = "name")
    private String name; //nome do user. Quando entra com a conta do google esta ja possui um nome para o utilizador

    @ColumnInfo(name = "email")
    private String email; //email do utilizador. Email da conta google

    @ColumnInfo(name = "photo_url")
    private String photo_url; //foto do utilizador. Foto da conta google. Se a conta google não tiver conta é usada a drawable "treinador.png"

    @ColumnInfo(name = "experiencia")
    private long experiencia; //experiência do utilizador. Quando inicia o jogo pela primeira vez a experiência é 0, mas à medida que apanha pokemons o utilizdor vai acumulando experiência e subindo de nivel

    @ColumnInfo(name = "itens")
    private String itens; //itens do utilizador. O utilizador possui 4 tipos de pokebolas para apanhar Pokemons. Esta variável guarda a quantidade de pokebolas de cada tipo, no formato "23:12:5:78"

    @ColumnInfo(name = "data_inicio")
    private long data_inicio; //data (tempo de milisegundos) do momento em que o utilizador usou a conta na app pela primeira vez

    @ColumnInfo(name = "itens_time")
    private long itens_time; //data da ultima vez que o utilizador recebeu itens.

    public User() {
    }

    @Ignore
    public User(String name, String email, String photo_url) {
        this.name = name;
        this.email = email;
        this.photo_url = photo_url; //guarda o url da foto do google, se nao tiver a string fica em branco
        this.experiencia = 0; //user começa com 0 de experiência quando abre a app pela primeira vez
        this.itens = "0:0:0:0"; //o utilizador começa com 10 pokebolas de cada tipo
        this.data_inicio = Calendar.getInstance().getTimeInMillis(); //guarda a data de quando o user foi criado
        this.itens_time = 0;

    }

    /**
     * a data que é guardada está em milissegundos e a função devolve uma string no formato DD/MM/AAAA, correspondente ao tempo em milissegundos da data
     *
     * @return string no formato DD/MM/AAAA da data, convertida de milisegundos para dia, mes e ano correspondente
     */
    public String getDataInicioToString() {
        final Calendar cal = Calendar.getInstance();
        final int dia = cal.get(Calendar.DAY_OF_MONTH);
        final int mes = cal.get(Calendar.MONTH) + 1;
        final int ano = cal.get(Calendar.YEAR);
        String data = dia + "/" + mes + "/" + ano;
        if (dia < 10 && mes < 10)
            data = "0" + dia + "/0" + mes + "/" + ano;

        if (dia >= 10 && mes < 10)
            data = dia + "/0" + mes + "/" + ano;

        if (dia < 10 && mes >= 10)
            data = "0" + dia + "/" + mes + "/" + ano;
        return data;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public long getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(long experiencia) {
        this.experiencia = experiencia;
    }

    public String getItens() {
        return itens;
    }

    public void setItens(String itens) {
        this.itens = itens;
    }

    public long getData_inicio() {
        return data_inicio;
    }

    public void setData_inicio(long data_inicio) {
        this.data_inicio = data_inicio;
    }

    public long getItens_time() {
        return itens_time;
    }

    public void setItens_time(long itens_time) {
        this.itens_time = itens_time;
    }

    public int[] getArrayItens() {
        String[] array = itens.split(":");
        int[] pokebolas = new int[4];
        pokebolas[0] = Integer.parseInt(array[0]);
        pokebolas[1] = Integer.parseInt(array[1]);
        pokebolas[2] = Integer.parseInt(array[2]);
        pokebolas[3] = Integer.parseInt(array[3]);
        return pokebolas;
    }

    /**
     * Override do metodo toString para imprimir o mais importante do objeto User
     *
     * @return string com algumas informaçoes do Objeto User
     */
    @Override
    public String toString() {
        return new StringBuilder("Nome: " + name).append("\nEmail: " + email).append("\nData Inicio: " + data_inicio).append("\nExperiencia total: " + experiencia).append("\nItens: " + itens).toString();
    }
}
