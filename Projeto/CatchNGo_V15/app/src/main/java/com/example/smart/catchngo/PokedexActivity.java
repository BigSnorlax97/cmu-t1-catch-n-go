package com.example.smart.catchngo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class PokedexActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView textView_gen_1, textView_gen_2, textView_gen_3, textView_gen_4, textView_gen_5, textView_gen_6, textView_gen_7;
    private LinearLayout linearLayout_1;
    public static String NUM = "";
    private LinearLayout lin;
    private int num = 1, gen_num = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokedex);

        textView_gen_1 = findViewById(R.id.textView_gen_1);
        textView_gen_2 = findViewById(R.id.textView_gen_2);
        textView_gen_3 = findViewById(R.id.textView_gen_3);
        textView_gen_4 = findViewById(R.id.textView_gen_4);
        textView_gen_5 = findViewById(R.id.textView_gen_5);
        textView_gen_6 = findViewById(R.id.textView_gen_6);
        textView_gen_7 = findViewById(R.id.textView_gen_7);

        linearLayout_1 = findViewById(R.id.linearLayout_1);

        textView_gen_1.setOnClickListener(this);
        textView_gen_2.setOnClickListener(this);
        textView_gen_3.setOnClickListener(this);
        textView_gen_4.setOnClickListener(this);
        textView_gen_5.setOnClickListener(this);
        textView_gen_6.setOnClickListener(this);
        textView_gen_7.setOnClickListener(this);

        setLayouts();

        //mostrar todos os Pokemons de todas as geraçoes
        showGen(1, 31, 1, 151);
        showGen(32, 52, 152, 251);
        showGen(53, 80, 252, 386);
        showGen(81, 103, 387, 493);
        showGen(104, 136, 494, 649);
        showGen(137, 152, 650, 721);
        showGen(153, 170, 722, 802);

    }

    /**
     * Mostra todos os Pokemons de uma geração
     * Mostra os Pokemon entre num_ini e num_fim
     * Apenas funciona se receber os layouts correspondeste ao numero dos Pokemon
     * Cada layout tem o nome da geraçao ou 5 Pokemon (ou menos de 5 Pokemons)
     * Este metodo cria os imageView e insere-os nos layouts, de forma a criar um layout intuitivo e mais à frente poder mostrar ou não os Pokemons por geraçoes
     *
     * @param lin_ini - layout inicial
     * @param lin_fim - layout final
     * @param num_ini - numero inicial
     * @param num_fim - numero final
     */
    public void showGen(int lin_ini, int lin_fim, int num_ini, int num_fim) {
        for (int i = lin_ini; i <= lin_fim; i++) {
            for (int j = 1; j <= 5; j++) {
                if (num >= num_ini && num <= num_fim) {
                    ImageView image = new ImageView(PokedexActivity.this);
                    image.setId(num);
                    image.setOnClickListener(this);
                    image.setBackground(getDrawable(R.drawable.fundo_2));
                    if (num == 1 || num == 152 || num == 252 || num == 387 || num == 494 || num == 650 || num == 722) {
                        lin = findViewById(1000 + i + gen_num);
                        TextView textView = new TextView(PokedexActivity.this);
                        textView.setText("---------- Gen " + (gen_num + 1) + " ----------");
                        textView.setTextSize(16);
                        textView.setId(2000 + gen_num);
                        lin.addView(textView);
                        gen_num++;
                    }
                    lin = findViewById(1000 + i + gen_num);
                    lin.addView(image);
                    String image_url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + num + ".png";
                    Picasso.get().load(image_url).into(image);
                    num++;
                }
            }
        }
    }

    /**
     * Cria todos os layout (linhas) necessarias para mais tarde introduzir as imagens dos Pokemon
     */
    public void setLayouts() {
        for (int i = 1; i <= 187; i++) {
            LinearLayout linearLayout = new LinearLayout(PokedexActivity.this);
            linearLayout.setGravity(1);
            linearLayout.setId(1000 + i);
            linearLayout_1.addView(linearLayout);
        }
    }

    public void boldStyleRemove() {
        textView_gen_1.setTypeface(Typeface.DEFAULT);
        textView_gen_2.setTypeface(Typeface.DEFAULT);
        textView_gen_3.setTypeface(Typeface.DEFAULT);
        textView_gen_4.setTypeface(Typeface.DEFAULT);
        textView_gen_5.setTypeface(Typeface.DEFAULT);
        textView_gen_6.setTypeface(Typeface.DEFAULT);
        textView_gen_7.setTypeface(Typeface.DEFAULT);
    }

    /**
     * Fazer os layout indesejados desaparecer (entre first e last)
     *
     * @param first - primeiro layout a desaparecer
     * @param last  - ultimos layout a desaparecer
     */
    public void setLayoutsGone(int first, int last) {
        for (int i = first; i <= last; i++) {
            LinearLayout lin_2 = findViewById(1000 + i);
            lin_2.setVisibility(View.GONE);
        }
    }

    /**
     * Tornar os layouts desejados visiveis (entre first e last)
     *
     * @param first - primeiro layout a aparecer
     * @param last  - ultimo layout a aparecer
     */
    public void setLayoutsVisible(int first, int last) {
        for (int i = first; i <= last; i++) {
            LinearLayout lin_2 = findViewById(1000 + i);
            lin_2.setVisibility(View.VISIBLE);
        }
    }

    @SuppressLint("ResourceType")
    @Override
    public void onClick(View v) {
        boldStyleRemove();

        if (v.getId() == R.id.textView_gen_1) { //mostra os Pokemon da 1ª geração (1 - 151)
            textView_gen_1.setTypeface(Typeface.DEFAULT_BOLD);
            TextView textView = findViewById(2000);
            textView.setText("---------- Gen 1 ----------");
            setLayoutsVisible(1, 32);
            setLayoutsGone(33, 187);
        } else if (v.getId() == R.id.textView_gen_2) { //mostra os Pokemon da 2ª geração (152 - 251)
            textView_gen_2.setTypeface(Typeface.DEFAULT_BOLD);
            TextView textView = findViewById(2000);
            textView.setText("---------- Gen 2 ----------");
            setLayoutsVisible(34, 53);
            setLayoutsGone(2, 33);
            setLayoutsGone(54, 187);
        } else if (v.getId() == R.id.textView_gen_3) { //mostra os Pokemon da 3ª geração (252 - 386)
            textView_gen_3.setTypeface(Typeface.DEFAULT_BOLD);
            TextView textView = findViewById(2000);
            textView.setText("---------- Gen 3 ----------");
            setLayoutsVisible(56, 82);
            setLayoutsGone(2, 55);
            setLayoutsGone(83, 187);
        } else if (v.getId() == R.id.textView_gen_4) { //mostra os Pokemon da 4ª geração (387 - 493)
            textView_gen_4.setTypeface(Typeface.DEFAULT_BOLD);
            TextView textView = findViewById(2000);
            textView.setText("---------- Gen 4 ----------");
            setLayoutsVisible(85, 106);
            setLayoutsGone(2, 84);
            setLayoutsGone(107, 187);
        } else if (v.getId() == R.id.textView_gen_5) { //mostra os Pokemon da 5ª geração (494 - 649)
            textView_gen_5.setTypeface(Typeface.DEFAULT_BOLD);
            TextView textView = findViewById(2000);
            textView.setText("---------- Gen 5 ----------");
            setLayoutsVisible(109, 140);
            setLayoutsGone(2, 108);
            setLayoutsGone(141, 187);
        } else if (v.getId() == R.id.textView_gen_6) { //mostra os Pokemon da 6ª geração (650 - 721)
            textView_gen_6.setTypeface(Typeface.DEFAULT_BOLD);
            TextView textView = findViewById(2000);
            textView.setText("---------- Gen 6 ----------");
            setLayoutsVisible(143, 158);
            setLayoutsGone(2, 142);
            setLayoutsGone(159, 187);
        } else if (v.getId() == R.id.textView_gen_7) { //mostra os Pokemon da 7ª geração (722 - 802)
            textView_gen_7.setTypeface(Typeface.DEFAULT_BOLD);
            TextView textView = findViewById(2000);
            textView.setText("---------- Gen 7 ----------");
            setLayoutsVisible(160, 187);
            setLayoutsGone(2, 159);
        } else {
            //ao clicar num Pokemon é iniciada a atividade que mostra os dados desse Pokemon
            NUM = String.valueOf(v.getId());
            Intent intent = new Intent(this, PokemonIndividualActivity.class);
            startActivity(intent);
        }
    }
}
