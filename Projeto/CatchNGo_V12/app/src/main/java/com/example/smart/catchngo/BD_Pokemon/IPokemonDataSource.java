package com.example.smart.catchngo.BD_Pokemon;

import java.util.List;

import io.reactivex.Flowable;

public interface IPokemonDataSource {

    Flowable<Pokemon> getPokemonById(int userId);
    Flowable<List<Pokemon>> getAllPokemons();
    void insertPokemon(Pokemon... pokemons);
    void updatePokemon(Pokemon... pokemons);
    void deletePokemon(Pokemon pokemon);
    void deleteAllPokemons();

}
