package com.example.smart.catchngo.BD_Pokemon;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import static com.example.smart.catchngo.BD_Pokemon.PokemonDatabase.DATABASE_VERSION;

@Database(entities = Pokemon.class, version = DATABASE_VERSION)
public abstract class PokemonDatabase extends RoomDatabase {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "9-Database-Room";

    public abstract PokemonDAO pokemonDAO();

    private static PokemonDatabase mInstance;

    public static PokemonDatabase getInstance(Context context){
        if(mInstance == null){
            mInstance = Room.databaseBuilder(context, PokemonDatabase.class, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return mInstance;
    }

}
