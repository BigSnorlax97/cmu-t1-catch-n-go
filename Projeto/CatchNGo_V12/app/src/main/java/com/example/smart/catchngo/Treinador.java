package com.example.smart.catchngo;

public class Treinador {

    private String nome;
    private String email;
    private String photo_url;

    public Treinador() {
    }

    public Treinador(String nome, String email, String photo_url) {
        this.nome = nome;
        this.email = email;
        this.photo_url = photo_url;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }
}
