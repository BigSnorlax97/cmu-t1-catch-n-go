package com.example.smart.catchngo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class ItemsActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView textView_pokeball, textView_greatball, textView_ultraball, textView_masterball;
    private Button button_receber_items;
    private int pokeballs, greatballs, ultraballs, masterballs;
    private String[] items = new String[4];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items);

        textView_pokeball = findViewById(R.id.item_pokeball);
        textView_greatball = findViewById(R.id.item_greatball);
        textView_ultraball = findViewById(R.id.item_ultraball);
        textView_masterball = findViewById(R.id.item_masterball);
        button_receber_items = findViewById(R.id.button_recebe_items);

        items = MenuActivity.ITEMS.split(":");

        pokeballs = Integer.parseInt(items[0]);
        greatballs = Integer.parseInt(items[1]);
        ultraballs = Integer.parseInt(items[2]);
        masterballs = Integer.parseInt(items[3]);

        textView_pokeball.setText("Pokeball X " + pokeballs);
        textView_greatball.setText("Greatball X " + greatballs);
        textView_ultraball.setText("Ultraball X " + ultraballs);
        textView_masterball.setText("Masterball X " + masterballs);

        button_receber_items.setOnClickListener(this);

    }

    //receber itens
    public void adicionarPokeBolas() {
        Random rnd = new Random();
        pokeballs += rnd.nextInt(6) + 12;
        textView_pokeball.setText("Pokeball X " + pokeballs);
        greatballs += rnd.nextInt(3) + 8;
        textView_greatball.setText("Greatball X " + greatballs);
        ultraballs += rnd.nextInt(2) + 4;
        textView_ultraball.setText("Ultraball X " + ultraballs);
        masterballs += rnd.nextInt(2);
        textView_masterball.setText("Masterball X " + masterballs);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_recebe_items) {
            adicionarPokeBolas();
        }
    }
}
