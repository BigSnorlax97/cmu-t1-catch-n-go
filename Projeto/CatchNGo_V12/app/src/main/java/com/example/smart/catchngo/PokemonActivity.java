package com.example.smart.catchngo;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smart.catchngo.BD.User;
import com.example.smart.catchngo.BD_Pokemon.Pokemon;
import com.example.smart.catchngo.BD_Pokemon.PokemonDataSource;
import com.example.smart.catchngo.BD_Pokemon.PokemonDatabase;
import com.example.smart.catchngo.BD_Pokemon.PokemonRepository;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class PokemonActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView image;
    private LinearLayout linearLayout_1;
    private TextView textView_espaco_pokemon;
    private ProgressBar progressBar_pokemon;

    //Adapter
    private List<Pokemon> pokemonList = new ArrayList<>();
    private ArrayAdapter adapter;

    //Database
    private CompositeDisposable compositeDisposable;
    private PokemonRepository pokemonRepository;

    private int espaco = 0;

    private ListView listView_pokemons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon);

        textView_espaco_pokemon = findViewById(R.id.textView_espaco_pokemon);
        progressBar_pokemon = findViewById(R.id.progressbar_pokemon);

/*
        int num;
        linearLayout_1 = findViewById(R.id.linearLayout_1);
        Random rnd = new Random();

        for (int i = 1; i <= 25; i++) {
            LinearLayout linearLayout = new LinearLayout(PokemonActivity.this);
            linearLayout.setGravity(1);
            linearLayout_1.addView(linearLayout);
            for (int j = 1; j <= 5; j++) {
                ImageView image = new ImageView(PokemonActivity.this);
                linearLayout.addView(image);
                num = rnd.nextInt(802) + 1;
                if (num <= 802 && num > 0) {
                    String image_url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + num + ".png";
                    Picasso.get().load(image_url).into(image);
                    espaco++;
                }
            }
        }
*/

        listView_pokemons = findViewById(R.id.listView_pokemons);

        adapter = new ArrayAdapter(PokemonActivity.this, android.R.layout.simple_list_item_1, pokemonList);
        registerForContextMenu(listView_pokemons);
        listView_pokemons.setAdapter(adapter);

        //Init
        compositeDisposable = new CompositeDisposable();

        //Database
        PokemonDatabase pokemonDatabase = PokemonDatabase.getInstance(this); //create database
        pokemonRepository = PokemonRepository.getInstance(PokemonDataSource.getInstance(pokemonDatabase.pokemonDAO()));

        //Load all data from Database
        loadData();

        textView_espaco_pokemon.setText(espaco + "/400");
        progressBar_pokemon.setProgress(espaco / 4 + espaco % 4);

        Runnable r = new Runnable() {
            @Override
            public void run(){
                textView_espaco_pokemon.setText(espaco + "/400");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    progressBar_pokemon.setProgress(espaco / 4 + espaco % 4, true);
                }
            }
        };
        Handler h = new Handler();
        h.postDelayed(r, 1000);

    }

    private void loadData() {
        //Use RxJava
        Disposable disposable = pokemonRepository.getAllPokemons()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<Pokemon>>() {
                    @Override
                    public void accept(List<Pokemon> pokemons) throws Exception {
                        OnGetAllPokemonSuccess(pokemons);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(PokemonActivity.this, "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        compositeDisposable.add(disposable);
    }

    private void OnGetAllPokemonSuccess(List<Pokemon> pokemons) {
        pokemonList.clear();
        pokemonList.addAll(pokemons);
        adapter.notifyDataSetChanged();
        espaco = pokemons.size();
    }

    @Override
    public void onClick(View v) {
        textView_espaco_pokemon.setText(espaco + "/400");
        progressBar_pokemon.setProgress(espaco / 4 + espaco % 4);
    }
}
