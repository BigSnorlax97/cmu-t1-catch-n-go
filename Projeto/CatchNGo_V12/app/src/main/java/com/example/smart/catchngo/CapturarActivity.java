package com.example.smart.catchngo;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.smart.catchngo.BD_Pokemon.Pokemon;
import com.example.smart.catchngo.BD_Pokemon.PokemonDataSource;
import com.example.smart.catchngo.BD_Pokemon.PokemonDatabase;
import com.example.smart.catchngo.BD_Pokemon.PokemonRepository;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class CapturarActivity extends AppCompatActivity implements View.OnClickListener {

    private Button button_capturar_pokemon;
    private ImageView imageView_pokemon, imageView_change_ball;
    private TextView textView_pokemon_nome, textView_pokemon_nivel, textView_location, textView_sucesso_captura;
    private int ball = 1, bcr = -1;
    private String nome = "";
    private Pokemon pokemon = new Pokemon();

    //Location
    private LocationManager locationManager;
    private LocationListener locationListener;
    private String locationProvider = LocationManager.NETWORK_PROVIDER;

    //Adapter
    private List<Pokemon> pokemonList = new ArrayList<>();
    private ArrayAdapter adapter;

    //Database
    private CompositeDisposable compositeDisposable;
    private PokemonRepository pokemonRepository;

    private Random rnd = new Random();
    private boolean gotFirstLocation = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capturar);

        button_capturar_pokemon = findViewById(R.id.button_capturar_pokemon);
        imageView_pokemon = findViewById(R.id.imageView_pokemon);
        textView_pokemon_nome = findViewById(R.id.textView_pokemon_nome);
        textView_pokemon_nivel = findViewById(R.id.textView_pokemon_nivel);
        imageView_change_ball = findViewById(R.id.imageView_change_ball);
        textView_location = findViewById(R.id.textView_location);
        textView_sucesso_captura = findViewById(R.id.textView_sucesso_captura);

        button_capturar_pokemon.setOnClickListener(this);
        imageView_change_ball.setOnClickListener(this);

        //criar o pokemon a capturar
        Random rnd = new Random();
        int num = rnd.nextInt(802) + 1;
        int nivel = rnd.nextInt(30) + 5;

        pokemon.setNivel(nivel);
        pokemon.setNumero(num);
        pokemon.setImage_url();

        String url = "https://pokeapi.co/api/v2/pokemon/" + num + "/";
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    nome = response.getString("name");
                    pokemon.setName(nome.substring(0, 1).toUpperCase() + nome.substring(1));
                    mostrarPokemon();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jor);

        String url_2 = "https://pokeapi.co/api/v2/pokemon-species/" + num + "/";
        JsonObjectRequest jor_2 = new JsonObjectRequest(Request.Method.GET, url_2, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    bcr = Integer.parseInt(response.getString("capture_rate"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
            }
        });
        queue.add(jor_2);

        //Init
        compositeDisposable = new CompositeDisposable();

        //Database
        PokemonDatabase pokemonDatabase = PokemonDatabase.getInstance(this); //create database
        pokemonRepository = PokemonRepository.getInstance(PokemonDataSource.getInstance(pokemonDatabase.pokemonDAO()));

        //Load all data from Database
        loadData();

        button_capturar_pokemon.setEnabled(false);
        button_capturar_pokemon.setText("Wait Loading...");

        button_capturar_pokemon.setOnClickListener(CapturarActivity.this);

        imageView_change_ball.setOnClickListener(this);

        location();

    }

    private void loadData() {
        //Use RxJava
        Disposable disposable = pokemonRepository.getAllPokemons()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<Pokemon>>() {
                    @Override
                    public void accept(List<Pokemon> pokemons) throws Exception {
                        OnGetAllPokemonSuccess(pokemons);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //Toast.makeText(CapturarActivity.this, "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        compositeDisposable.add(disposable);
    }

    private void OnGetAllPokemonSuccess(List<Pokemon> pokemons) {
        pokemonList.clear();
        pokemonList.addAll(pokemons);
        adapter.notifyDataSetChanged();
    }

    public void mostrarPokemon() {
        textView_pokemon_nome.setText(pokemon.getName());
        textView_pokemon_nivel.setText("Nivel: " + String.valueOf(pokemon.getNivel()));
        Picasso.get().load(pokemon.getImage_url()).into(imageView_pokemon);
    }

    //muda de pokebola
    public void changePokeball() {
        if (ball == 1) {
            imageView_change_ball.setImageDrawable(getDrawable(R.drawable.greatball));
            ball = 2;
        } else if (ball == 2) {
            imageView_change_ball.setImageDrawable(getDrawable(R.drawable.ultraball));
            ball = 3;
        } else if (ball == 3) {
            imageView_change_ball.setImageDrawable(getDrawable(R.drawable.masterball));
            ball = 4;
        } else if (ball == 4) {
            imageView_change_ball.setImageDrawable(getDrawable(R.drawable.pokeball));
            ball = 1;
        } else {
            imageView_change_ball.setImageDrawable(getDrawable(R.drawable.zero));
            ball = 1;
        }
    }

    public void location() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                pokemon.setLatitude(String.valueOf(location.getLatitude()));
                pokemon.setLongitude(String.valueOf(location.getLongitude()));
                textView_location.setText("Lat: " + pokemon.getLatitude() + "\nLon: " + pokemon.getLongitude());
                if (!gotFirstLocation) {
                    gotFirstLocation = true;
                    button_capturar_pokemon.setEnabled(true);
                    button_capturar_pokemon.setText("Capturar");
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET
                }, 10);
                return;
            }
        } else {
            configureButton();
        }
        configureButton();
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 10:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    configureButton();
                return;
        }
    }

    private void configureButton() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        locationManager.requestLocationUpdates(locationProvider, 0, 0, locationListener);
    }

    private boolean capturarPokemon() {
        capturandoWait();
        if (ball == 4) {
            return true;
        } else {
            int number = 0;
            if (ball == 1) {
                number = (rnd.nextInt(200));
            } else if (ball == 2) {
                number = rnd.nextInt(150);
            } else if (ball == 3) {
                number = rnd.nextInt(100);
            } else {
                Toast.makeText(this, "Pokebola Inválida....", Toast.LENGTH_SHORT).show();
            }
            //Toast.makeText(this, "" + (bcr + 10) + "\n" + number, Toast.LENGTH_SHORT).show();
            if (number <= bcr + 10) {
                return true;
            }
        }
        return false;

    }

    private void capturandoWait() {
        button_capturar_pokemon.setEnabled(false);
        button_capturar_pokemon.setText("Capturando");
        textView_sucesso_captura.setText("");

        Runnable r = new Runnable() {
            @Override
            public void run() {
                button_capturar_pokemon.setText("Capturando.");
            }
        };
        Handler h = new Handler();
        h.postDelayed(r, 1000);

        r = new Runnable() {
            @Override
            public void run() {
                button_capturar_pokemon.setText("Capturando..");
            }
        };
        h = new Handler();
        h.postDelayed(r, 2000);

        r = new Runnable() {
            @Override
            public void run() {
                button_capturar_pokemon.setText("Capturando...");
            }
        };
        h = new Handler();
        h.postDelayed(r, 3000);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_capturar_pokemon) {
            //Add new pokemon
            if (capturarPokemon()) {
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        button_capturar_pokemon.setText("CAPTURADO!!!");
                        textView_sucesso_captura.setText("CAPTURADO!!!");
                    }
                };
                Handler h = new Handler();
                h.postDelayed(r, 4000);
                r = new Runnable() {
                    @Override
                    public void run() {
                        Disposable disposable = Observable.create(new ObservableOnSubscribe<Object>() {
                            @Override
                            public void subscribe(ObservableEmitter<Object> e) throws Exception {
                                pokemonRepository.insertPokemon(pokemon);
                                e.onComplete();
                            }
                        })
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.io())
                                .subscribe(new Consumer<Object>() {
                                    @Override
                                    public void accept(Object o) throws Exception {
                                        //Toast.makeText(CapturarActivity.this, "Pokemon added!!!", Toast.LENGTH_SHORT).show();
                                    }
                                }, new Consumer<Throwable>() {
                                    @Override
                                    public void accept(Throwable throwable) throws Exception {
                                        Toast.makeText(CapturarActivity.this, "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }, new Action() {
                                    @Override
                                    public void run() throws Exception {
                                        loadData(); //Refresh data
                                    }
                                });
                        startActivity(new Intent(CapturarActivity.this, MenuActivity.class));
                    }
                };
                h = new Handler();
                h.postDelayed(r, 5000);
            } else {
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        button_capturar_pokemon.setText("Captura falhada");
                        textView_sucesso_captura.setText("Captura falhada");
                    }
                };
                Handler h = new Handler();
                h.postDelayed(r, 4000);
                r = new Runnable() {
                    @Override
                    public void run() {
                        button_capturar_pokemon.setEnabled(true);
                        button_capturar_pokemon.setText("Capturar");
                    }
                };
                h = new Handler();
                h.postDelayed(r, 5000);
            }
        }
        if (v.getId() == R.id.imageView_change_ball) {
            changePokeball();
        }
    }
}
