package com.example.smart.catchngo.BD_Pokemon;

import java.util.List;

import io.reactivex.Flowable;

public class PokemonRepository implements IPokemonDataSource {

    private IPokemonDataSource mLocalDataSource;

    private static PokemonRepository mInstance;

    public PokemonRepository(IPokemonDataSource mLocalDataSource) {
        this.mLocalDataSource = mLocalDataSource;
    }

    public static PokemonRepository getInstance(IPokemonDataSource mLocalDataSource) {
        if (mInstance == null) {
            mInstance = new PokemonRepository(mLocalDataSource);
        }
        return mInstance;
    }

    @Override
    public Flowable<Pokemon> getPokemonById(int pokemonId) {
        return mLocalDataSource.getPokemonById(pokemonId);
    }

    @Override
    public Flowable<List<Pokemon>> getAllPokemons() {
        return mLocalDataSource.getAllPokemons();
    }

    @Override
    public Pokemon getPokemonTrainer(int i) {
        return mLocalDataSource.getPokemonTrainer(i);
    }

    @Override
    public void insertPokemon(Pokemon... pokemons) {
        mLocalDataSource.insertPokemon(pokemons);
    }

    @Override
    public void updatePokemon(Pokemon... pokemons) {
        mLocalDataSource.updatePokemon(pokemons);
    }

    @Override
    public void deletePokemon(Pokemon pokemon) {
        mLocalDataSource.deletePokemon(pokemon);
    }

    @Override
    public void deleteAllPokemons() {
        mLocalDataSource.deleteAllPokemons();
    }
}
