package com.example.smart.catchngo.BD;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Calendar;

@Entity(tableName = "users")
public class User {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "email")
    private String email;

    @ColumnInfo(name = "photo_url")
    private String photo_url;

    @ColumnInfo(name = "experiencia")
    private long experiencia;

    @ColumnInfo(name = "itens")
    private String itens;

    @ColumnInfo(name = "data_inicio")
    private long data_inicio;

    @ColumnInfo(name = "itens_time")
    private long itens_time;

    public User() {
    }

    @Ignore
    public User(String name, String email, String photo_url) {
        this.name = name;
        this.email = email;
        this.photo_url = photo_url;
        this.experiencia = 0;
        this.itens = "10:10:10:10";
        this.data_inicio = Calendar.getInstance().getTimeInMillis();
        this.itens_time = 0;

    }

    public String getDataInicioToString() {
        final Calendar cal = Calendar.getInstance();
        final int dia = cal.get(Calendar.DAY_OF_MONTH);
        final int mes = cal.get(Calendar.MONTH) + 1;
        final int ano = cal.get(Calendar.YEAR);
        String data = dia + "/" + mes + "/" + ano;
        if (dia < 10 && mes < 10)
            data = "0" + dia + "/0" + mes + "/" + ano;

        if (dia >= 10 && mes < 10)
            data = dia + "/0" + mes + "/" + ano;

        if (dia < 10 && mes >= 10)
            data = "0" + dia + "/" + mes + "/" + ano;
        return data;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(long experiencia) {
        this.experiencia = experiencia;
    }

    public String getItens() {
        return itens;
    }

    public void setItens(String itens) {
        this.itens = itens;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public long getData_inicio() {
        return data_inicio;
    }

    public void setData_inicio(long data_inicio) {
        this.data_inicio = data_inicio;
    }

    public long getItens_time() {
        return itens_time;
    }

    public void setItens_time(long itens_time) {
        this.itens_time = itens_time;
    }

    @Override
    public String toString() {
        return new StringBuilder(name).append("\n" + email).append("\n" + experiencia).append("\n" + itens).toString();
    }
}
