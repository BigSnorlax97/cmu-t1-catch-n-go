package com.example.smart.catchngo;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smart.catchngo.BD.User;
import com.example.smart.catchngo.BD.UserDataSource;
import com.example.smart.catchngo.BD.UserDatabase;
import com.example.smart.catchngo.BD.UserRepository;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ItemsActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView textView_pokeball, textView_greatball, textView_ultraball, textView_masterball;
    private Button button_receber_items;
    private int pokeballs = 0, greatballs = 0, ultraballs = 0, masterballs = 0;
    private String[] items = new String[4];
    private Runnable runnable;
    private Thread myThread = null;
    private long itens_time = 0;
    private User user;

    //Adapter
    private List<User> userList = new ArrayList<>();

    //Database
    private CompositeDisposable compositeDisposable;
    private UserRepository userRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items);

        textView_pokeball = findViewById(R.id.item_pokeball);
        textView_greatball = findViewById(R.id.item_greatball);
        textView_ultraball = findViewById(R.id.item_ultraball);
        textView_masterball = findViewById(R.id.item_masterball);
        button_receber_items = findViewById(R.id.button_recebe_items);

        textView_pokeball.setText("Pokeball X " + pokeballs);
        textView_greatball.setText("Greatball X " + greatballs);
        textView_ultraball.setText("Ultraball X " + ultraballs);
        textView_masterball.setText("Masterball X " + masterballs);

        button_receber_items.setOnClickListener(this);
        button_receber_items.setVisibility(View.INVISIBLE);

        //Init
        compositeDisposable = new CompositeDisposable();

        //Database
        UserDatabase userDatabase = UserDatabase.getInstance(this); //create database
        userRepository = UserRepository.getInstance(UserDataSource.getInstance(userDatabase.userDAO()));

        loadData();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                findUser();
                carregarItens();

                runnable = new CountDownRunner();
                myThread = new Thread(runnable);
                myThread.start();

                button_receber_items.setVisibility(View.VISIBLE);
            }
        };
        Handler h = new Handler();
        h.postDelayed(r, 1000);

    }

    public void carregarItens() {
        String itens = user.getItens();
        pokeballs = Integer.parseInt(itens.split(":")[0]);
        greatballs = Integer.parseInt(itens.split(":")[1]);
        ultraballs = Integer.parseInt(itens.split(":")[2]);
        masterballs = Integer.parseInt(itens.split(":")[3]);
        textView_pokeball.setText("Pokeball X " + pokeballs);
        textView_greatball.setText("Greatball X " + greatballs);
        textView_ultraball.setText("Ultraball X " + ultraballs);
        textView_masterball.setText("Masterball X " + masterballs);

        itens_time = user.getItens_time();
    }

    private void loadData() {
        //Use RxJava
        Disposable disposable = userRepository.getAllUsers()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<User>>() {
                    @Override
                    public void accept(List<User> users) throws Exception {
                        onGetAllUserSuccess(users);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(ItemsActivity.this, "fail\n" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        compositeDisposable.add(disposable);
    }

    private void onGetAllUserSuccess(List<User> users) {
        userList.clear();
        userList.addAll(users);
    }

    public void findUser() {
        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).getEmail().equals(LoginActivity.EMAIL)) {
                user = userList.get(i);
            }
        }
    }

    public void updateUser(final User user) {
        Disposable disposable = Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> e) throws Exception {
                userRepository.updateUser(user);
                e.onComplete();

            }
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object o) throws Exception {

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(ItemsActivity.this, "2" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, new Action() {
                    @Override
                    public void run() throws Exception {
                        loadData(); //Refresh data
                    }
                });
        compositeDisposable.add(disposable);
    }

    //receber itens
    public void adicionarPokeBolas() {
        Random rnd = new Random();
        pokeballs += rnd.nextInt(1) + 1;
        textView_pokeball.setText("Pokeball X " + pokeballs);
        greatballs += rnd.nextInt(3) + 8;
        textView_greatball.setText("Greatball X " + greatballs);
        ultraballs += rnd.nextInt(2) + 4;
        textView_ultraball.setText("Ultraball X " + ultraballs);
        masterballs += rnd.nextInt(2);
        textView_masterball.setText("Masterball X " + masterballs);
    }

    public void notification() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        Intent notificationIntent = new Intent(this, AlarmReceiver.class);
        PendingIntent broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 300);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);
    }

    class CountDownRunner implements Runnable {
        // @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    doWork();
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                } catch (Exception e) {
                }
            }
        }
    }

    public void doWork() {
        runOnUiThread(new Runnable() {
            public void run() {
                try {
                    if (Calendar.getInstance().getTimeInMillis() > itens_time + 300000) {
                        button_receber_items.setEnabled(true);
                        button_receber_items.setText(" Receber Itens ");
                    } else {
                        button_receber_items.setEnabled(false);
                        button_receber_items.setText(" Pode receber mais itens daqui a 5 minutos ");
                    }
                } catch (Exception e) {
                }
            }
        });
    }

    @Override
    protected void onStop() {
        myThread.interrupt();
        super.onStop();
    }

    public void guardarItens() {
        String itens = pokeballs + ":" + greatballs + ":" + ultraballs + ":" + masterballs;
        user.setItens(itens);
        user.setItens_time(itens_time);
        updateUser(user);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_recebe_items) {
            adicionarPokeBolas();
            notification();
            itens_time = Calendar.getInstance().getTimeInMillis();
            guardarItens();
        }
    }
}
