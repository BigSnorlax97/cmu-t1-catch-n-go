package com.example.smart.catchngo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class PokedexActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView textView_gen_1, textView_gen_2, textView_gen_3, textView_gen_4, textView_gen_5, textView_gen_6, textView_gen_7;
    private LinearLayout linearLayout_1;
    public static String NUM = "";
    private ScrollView scrollView_pokedex;
    private LinearLayout lin;
    private int num = 1, gen_num = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokedex);

        textView_gen_1 = findViewById(R.id.textView_gen_1);
        textView_gen_2 = findViewById(R.id.textView_gen_2);
        textView_gen_3 = findViewById(R.id.textView_gen_3);
        textView_gen_4 = findViewById(R.id.textView_gen_4);
        textView_gen_5 = findViewById(R.id.textView_gen_5);
        textView_gen_6 = findViewById(R.id.textView_gen_6);
        textView_gen_7 = findViewById(R.id.textView_gen_7);

        linearLayout_1 = findViewById(R.id.linearLayout_1);

        scrollView_pokedex = findViewById(R.id.scrollView_pokedex);

        textView_gen_1.setOnClickListener(this);
        textView_gen_2.setOnClickListener(this);
        textView_gen_3.setOnClickListener(this);
        textView_gen_4.setOnClickListener(this);
        textView_gen_5.setOnClickListener(this);
        textView_gen_6.setOnClickListener(this);
        textView_gen_7.setOnClickListener(this);

        //allTheDex();

        setLayouts();
        showGen(1, 31, 1, 151);
        showGen(32, 52, 152, 251);
        showGen(53, 80, 252, 386);
        showGen(81, 103, 387, 493);
        showGen(104, 136, 494, 649);
        showGen(137, 152, 650, 721);
        showGen(153, 170, 722, 802);

        //change image from textView (left, top, right, bottom)
        //textView_gen_1.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ball_1, 0, 0);

    }

    public void showGen(int lin_ini, int lin_fim, int num_ini, int num_fim) {
        for (int i = lin_ini; i <= lin_fim; i++) {
            for (int j = 1; j <= 5; j++) {
                if (num >= num_ini && num <= num_fim) {
                    ImageView image = new ImageView(PokedexActivity.this);
                    image.setId(num);
                    image.setOnClickListener(this);
                    image.setBackground(getDrawable(R.drawable.fundo_2));
                    if (num == 1 || num == 152 || num == 252 || num == 387 || num == 494 || num == 650 || num == 722) {
                        lin = findViewById(1000 + i + gen_num);
                        TextView textView = new TextView(PokedexActivity.this);
                        textView.setText("---------- Gen " + (gen_num + 1) + " ----------");
                        textView.setTextSize(16);
                        //textView.setTextColor(255);
                        lin.addView(textView);
                        gen_num++;
                    }
                    lin = findViewById(1000 + i + gen_num);
                    lin.addView(image);
                    String image_url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + num + ".png";
                    Picasso.get().load(image_url).into(image);
                    num++;
                }
            }
        }
    }

    public void setLayouts() {
        for (int i = 1; i <= 187; i++) {
            LinearLayout linearLayout = new LinearLayout(PokedexActivity.this);
            linearLayout.setGravity(1);
            linearLayout.setId(1000 + i);
            linearLayout_1.addView(linearLayout);
        }
    }

    public void boldStyleRemove() {
        textView_gen_1.setTypeface(Typeface.DEFAULT);
        textView_gen_2.setTypeface(Typeface.DEFAULT);
        textView_gen_3.setTypeface(Typeface.DEFAULT);
        textView_gen_4.setTypeface(Typeface.DEFAULT);
        textView_gen_5.setTypeface(Typeface.DEFAULT);
        textView_gen_6.setTypeface(Typeface.DEFAULT);
        textView_gen_7.setTypeface(Typeface.DEFAULT);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @SuppressLint("ResourceType")
    @Override
    public void onClick(View v) {
        boldStyleRemove();
        if (v.getId() == R.id.textView_gen_1) {
            textView_gen_1.setTypeface(Typeface.DEFAULT_BOLD);
            scrollView_pokedex.scrollTo(0, 0);
            for (int i = 2; i <= 33; i++) {
                LinearLayout lin_2 = findViewById(1000 + i);
                lin_2.setVisibility(View.GONE);
            }
        } else if (v.getId() == R.id.textView_gen_2) {
            textView_gen_2.setTypeface(Typeface.DEFAULT_BOLD);
            scrollView_pokedex.scrollTo(0, 3505);
        } else if (v.getId() == R.id.textView_gen_3) {
            textView_gen_3.setTypeface(Typeface.DEFAULT_BOLD);
            scrollView_pokedex.scrollTo(0, 5780);
        } else if (v.getId() == R.id.textView_gen_4) {
            textView_gen_4.setTypeface(Typeface.DEFAULT_BOLD);
            scrollView_pokedex.scrollTo(0, 8840);
        } else if (v.getId() == R.id.textView_gen_5) {
            textView_gen_5.setTypeface(Typeface.DEFAULT_BOLD);
            scrollView_pokedex.scrollTo(0, 11335);
        } else if (v.getId() == R.id.textView_gen_6) {
            textView_gen_6.setTypeface(Typeface.DEFAULT_BOLD);
            scrollView_pokedex.scrollTo(0, 14950);
        } else if (v.getId() == R.id.textView_gen_7) {
            textView_gen_7.setTypeface(Typeface.DEFAULT_BOLD);
            scrollView_pokedex.scrollTo(0, 16660);
        } else {
            NUM = String.valueOf(v.getId());
            Intent intent = new Intent(this, PokemonIndividualActivity.class);
            intent.putExtra(NUM, 0);
            startActivity(intent);
        }
    }
}
