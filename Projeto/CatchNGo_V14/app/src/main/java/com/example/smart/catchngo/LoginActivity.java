package com.example.smart.catchngo;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smart.catchngo.BD.User;
import com.example.smart.catchngo.BD.UserDataSource;
import com.example.smart.catchngo.BD.UserDatabase;
import com.example.smart.catchngo.BD.UserRepository;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private SignInButton signInButton_login;
    private GoogleApiClient googleApiClient;
    private final int REQ_CODE = 9001;
    private TextView textView_time;
    private Intent intent;
    private boolean backAllow = false;
    private Runnable runnable;
    private Thread myThread = null;
    private String nome;
    private String email;
    private String photo_url;
    public static String EMAIL = "";

    //Adapter
    private List<User> userList = new ArrayList<>();

    //Database
    private CompositeDisposable compositeDisposable;
    private UserRepository userRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        signInButton_login = findViewById(R.id.signInButton_login);
        textView_time = findViewById(R.id.textView_time);
        signInButton_login.setOnClickListener(this);

        GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions).build();

        runnable = new CountDownRunner();
        myThread = new Thread(runnable);
        myThread.start();

        //Init
        compositeDisposable = new CompositeDisposable();

        //Database
        UserDatabase userDatabase = UserDatabase.getInstance(this); //create database
        userRepository = UserRepository.getInstance(UserDataSource.getInstance(userDatabase.userDAO()));

        loadData();
    }

    private void signIn() {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent, REQ_CODE);
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {

            }
        });
    }

    private void handleResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            nome = account.getDisplayName();
            email = account.getEmail();
            EMAIL = email;
            try {
                photo_url = account.getPhotoUrl().toString();
            } catch (Exception e) {
            }
            entrar();
        } else {
        }
    }

    private void loadData() {
        //Use RxJava
        Disposable disposable = userRepository.getAllUsers()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<User>>() {
                    @Override
                    public void accept(List<User> users) throws Exception {
                        onGetAllUserSuccess(users);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(LoginActivity.this, "fail\n" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        compositeDisposable.add(disposable);
    }

    private void onGetAllUserSuccess(List<User> users) {
        userList.clear();
        userList.addAll(users);
    }

    public void checkUsers() {
        //Add new user
        Disposable disposable = Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> e) throws Exception {
                User user = new User(nome, email, photo_url);
                boolean exists = false;
                for (int i = 0; i < userList.size(); i++) {
                    if (userList.get(i).getEmail().equals(user.getEmail())) {
                        exists = true;
                        message("Bem-vindo de volta!");
                    }
                }
                if (!exists) {
                    userRepository.insertUser(user);
                    message("Bem-vindo ao CatchNGo");
                }
                e.onComplete();
            }
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object o) throws Exception {
                        Toast.makeText(LoginActivity.this, "User added!!!", Toast.LENGTH_SHORT).show();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(LoginActivity.this, "Fail\n" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, new Action() {
                    @Override
                    public void run() throws Exception {
                        loadData();
                    }
                });
        compositeDisposable.add(disposable);
    }

    public void message(final String mes) {
        this.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(LoginActivity.this, mes, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (!backAllow) {
        } else {
            super.onBackPressed();
        }
    }

    class CountDownRunner implements Runnable {
        // @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    doWork();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                } catch (Exception e) {
                }
            }
        }
    }

    public void doWork() {
        runOnUiThread(new Runnable() {
            public void run() {
                try {
                    String horas = String.valueOf(Calendar.getInstance().getTime().getHours());
                    if (Integer.parseInt(horas) < 10) {
                        horas = "0" + horas;
                    }
                    String minutos = String.valueOf(Calendar.getInstance().getTime().getMinutes());
                    if (Integer.parseInt(minutos) < 10) {
                        minutos = "0" + minutos;
                    }
                    String segundos = String.valueOf(Calendar.getInstance().getTime().getSeconds());
                    if (Integer.parseInt(segundos) < 10) {
                        segundos = "0" + segundos;
                    }
                    textView_time.setText(horas + ":" + minutos + ":" + segundos);
                } catch (Exception e) {
                }
            }
        });
    }

    public void entrar() {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                checkUsers();
            }
        };
        Handler h = new Handler();
        h.postDelayed(r, 1000);
        r = new Runnable() {
            @Override
            public void run() {
                intent = new Intent(LoginActivity.this, MenuActivity.class);
                intent.putExtra(EMAIL, 0);
                startActivity(intent);
            }
        };
        h = new Handler();
        h.postDelayed(r, 2000);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.signInButton_login) {
            signOut();
            signIn();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleResult(result);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
