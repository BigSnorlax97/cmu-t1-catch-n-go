package com.example.smart.catchngo.BD_Pokemon;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface PokemonDAO {

    @Query("SELECT * FROM pokemons WHERE id=:pokemonId")
    Flowable<Pokemon> getPokemonById(int pokemonId);

    @Query("SELECT * FROM pokemons")
    Flowable<List<Pokemon>> getAllPokemons();

    @Query("SELECT * FROM pokemons WHERE id=:i")
    Pokemon getPokemonTrainer(int i);

    @Insert
    void insertPokemon(Pokemon... pokemons);

    @Update
    void updatePokemon(Pokemon... pokemons);

    @Delete
    void deletePokemon(Pokemon pokemon);

    @Query("DELETE FROM pokemons")
    void deleteAllPokemons();

}
