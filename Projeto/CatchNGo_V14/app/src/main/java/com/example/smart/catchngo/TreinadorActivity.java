package com.example.smart.catchngo;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smart.catchngo.BD.User;
import com.example.smart.catchngo.BD.UserDataSource;
import com.example.smart.catchngo.BD.UserDatabase;
import com.example.smart.catchngo.BD.UserRepository;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class TreinadorActivity extends AppCompatActivity {

    private ProgressBar progressBar_exp;
    private ImageView imageView_treinador_image;
    private TextView textView_nome_treinador, textView_nivel_treinador, textView_exp_valor, textView_data_inicio;
    private int[] niveis = {-1, 0, 1000, 3000, 6000, 10000, 15000, 21000, 28000, 36000, 45000, 55000, 65000, 75000, 85000, 100000, 120000, 140000, 160000, 185000, 210000, 260000, 335000, 435000, 560000, 710000, 900000, 1100000, 1350000, 1650000, 2000000, 2500000, 3000000, 3750000, 4750000, 6000000, 7500000, 9500000, 12000000, 15000000, 20000000};
    private long exp = 0;
    private User user;

    //Adapter
    private List<User> userList = new ArrayList<>();

    //Database
    private CompositeDisposable compositeDisposable;
    private UserRepository userRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_treinador);

        textView_nome_treinador = findViewById(R.id.textView_nome_treinador);
        textView_data_inicio = findViewById(R.id.textView_data_inicio);
        textView_exp_valor = findViewById(R.id.textView_exp_valor);
        imageView_treinador_image = findViewById(R.id.imageView_treinador_image);
        textView_nivel_treinador = findViewById(R.id.textView_nivel_treinador);
        progressBar_exp = findViewById(R.id.progressBar_exp);


        //Init
        compositeDisposable = new CompositeDisposable();

        //Database
        UserDatabase userDatabase = UserDatabase.getInstance(this); //create database
        userRepository = UserRepository.getInstance(UserDataSource.getInstance(userDatabase.userDAO()));

        loadData();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                findUser();
                textView_nome_treinador.setText(user.getName());
                textView_data_inicio.setText("Data de inicio  " + user.getDataInicioToString());
                if (user.getPhoto_url() != null) {
                    Picasso.get().load(user.getPhoto_url())
                            .error(R.drawable.treinador)
                            .into(imageView_treinador_image);
                } else {
                    imageView_treinador_image.setImageDrawable(getDrawable(R.drawable.treinador));
                }
                calcularNivel(user.getExperiencia(), niveis);
                textView_exp_valor.setText("Total XP  " + user.getExperiencia());
            }
        };
        Handler h = new Handler();
        h.postDelayed(r, 1000);
    }

    private void loadData() {
        //Use RxJava
        Disposable disposable = userRepository.getAllUsers()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<User>>() {
                    @Override
                    public void accept(List<User> users) throws Exception {
                        onGetAllUserSuccess(users);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(TreinadorActivity.this, "fail\n" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        compositeDisposable.add(disposable);
    }

    private void onGetAllUserSuccess(List<User> users) {
        userList.clear();
        userList.addAll(users);
    }

    public void findUser() {
        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).getEmail().equals(LoginActivity.EMAIL)) {
                user = userList.get(i);
            }
        }
    }

    private void calcularNivel(long exp, int[] niveis) {
        textView_exp_valor.setText("TOTAL XP  " + String.format("%,d", exp));
        if (exp > 20000000) {
            progressBar_exp.setVisibility(View.INVISIBLE);
            textView_nivel_treinador.setText("Nivel  40");
        } else {
            int i;
            for (i = 1; exp >= niveis[i]; i++) {
                textView_nivel_treinador.setText("Nivel  " + i);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                float progresso = (((float) (exp - niveis[i - 1])) / ((float) (niveis[i] - niveis[i - 1]))) * 100;
                progressBar_exp.setProgress((int) progresso, true);
            }
        }
    }
}
