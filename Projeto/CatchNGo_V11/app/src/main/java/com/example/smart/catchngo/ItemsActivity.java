package com.example.smart.catchngo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class ItemsActivity extends AppCompatActivity implements View.OnClickListener {

    TextView textView_pokeball, textView_greatball, textView_ultraball, textView_masterball;
    Button button_receber_itens;
    int pokeballs = 0, greatballs = 0, ultraballs = 0, masterballs = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items);

        textView_pokeball = findViewById(R.id.item_pokeball);
        textView_greatball = findViewById(R.id.item_greatball);
        textView_ultraball = findViewById(R.id.item_ultraball);
        textView_masterball = findViewById(R.id.item_masterball);
        button_receber_itens = findViewById(R.id.button_recebe_items);

        textView_pokeball.setText("Pokeball X " + pokeballs);
        textView_greatball.setText("Greatball X " + greatballs);
        textView_ultraball.setText("Ultraball X " + ultraballs);
        textView_masterball.setText("Masterball X " + masterballs);

        button_receber_itens.setOnClickListener(this);

    }

    //receber itens
    public void adicionarPokeBolas() {
        Random rnd = new Random();
        int ball = rnd.nextInt(4) + 1;
        switch (ball) {
            case 1:
                pokeballs += rnd.nextInt(9) + 2;
                textView_pokeball.setText("Pokeball X " + pokeballs);
                break;
            case 2:
                greatballs += rnd.nextInt(5) + 1;
                textView_greatball.setText("Greatball X " + greatballs);
                break;
            case 3:
                ultraballs += rnd.nextInt(2) + 1;
                textView_ultraball.setText("Ultraball X " + ultraballs);
                break;
            case 4:
                int n = rnd.nextInt(2);
                if(n != 0) {
                    masterballs += n;
                    textView_masterball.setText("Masterball X " + masterballs);
                }
            default:
                pokeballs += 1;
                textView_pokeball.setText("Pokeball X " + pokeballs);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_recebe_items) {
            adicionarPokeBolas();
        }
    }
}
