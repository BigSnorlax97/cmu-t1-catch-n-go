package com.example.smart.catchngo.BD_Pokemon;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "pokemons")
public class Pokemon {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "numero")
    private int numero;

    @ColumnInfo(name = "nivel")
    private int nivel;

    @ColumnInfo(name = "image")
    private String image_url;

    public Pokemon() {
    }

    @Ignore
    public Pokemon(String name, int numero, int nivel) {
        this.name = name;
        this.numero = numero;
        this.nivel = nivel;
        this.image_url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + numero + ".png";
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getImage_url() {
        return "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + numero + ".png";
    }

    public void setImage_url() {
        this.image_url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + numero + ".png";
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    @Override
    public String toString() {
        return new StringBuilder("id: " + id + "       Nome: " + name + "\nNº: " + numero + "     Nivel: " + nivel).toString();
    }
}
