package com.example.smart.catchngo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.smart.catchngo.BD_Pokemon.Pokemon;
import com.example.smart.catchngo.BD_Pokemon.PokemonDataSource;
import com.example.smart.catchngo.BD_Pokemon.PokemonDatabase;
import com.example.smart.catchngo.BD_Pokemon.PokemonRepository;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class CapturarActivity extends AppCompatActivity implements View.OnClickListener {

    Button button_Capturar_Pokemon;
    ImageView imageView_pokemon, imageView_change_ball;
    TextView textView_pokemon_nome, textView_pokemon_nivel;
    int ball = 1;
    String nome = "Pokemon X";
    Pokemon pokemon = new Pokemon();

    //Adapter
    List<Pokemon> pokemonList = new ArrayList<>();
    ArrayAdapter adapter;

    //Database
    private CompositeDisposable compositeDisposable;
    private PokemonRepository pokemonRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capturar);

        button_Capturar_Pokemon = findViewById(R.id.button_Capturar_Pokemon);
        imageView_pokemon = findViewById(R.id.imageView_pokemonImage);
        textView_pokemon_nome = findViewById(R.id.textView_pokemonName);
        textView_pokemon_nivel = findViewById(R.id.textView_Nivel);
        imageView_change_ball = findViewById(R.id.imageView_change_ball);

        button_Capturar_Pokemon.setOnClickListener(this);
        imageView_change_ball.setOnClickListener(this);

        //criar o pokemon a capturar
        Random rnd = new Random();
        int num = rnd.nextInt(802) + 1;
        int nivel = rnd.nextInt(30) + 5;

        pokemon.setNivel(nivel);
        pokemon.setNumero(num);
        pokemon.setImage_url();

        String url = "https://pokeapi.co/api/v2/pokemon/" + num + "/";
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    nome = response.getString("name");
                    pokemon.setName(nome.substring(0, 1).toUpperCase() + nome.substring(1));
                    mostrarPokemon();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jor);

        //Init
        compositeDisposable = new CompositeDisposable();

        //Database
        PokemonDatabase pokemonDatabase = PokemonDatabase.getInstance(this); //create database
        pokemonRepository = PokemonRepository.getInstance(PokemonDataSource.getInstance(pokemonDatabase.pokemonDAO()));

        //Load all data from Database
        loadData();

        Runnable r = new Runnable() {
            @Override
            public void run(){
                button_Capturar_Pokemon.setEnabled(true);
                button_Capturar_Pokemon.setText("Capturar");
            }
        };
        Handler h = new Handler();
        h.postDelayed(r, 3000);

        button_Capturar_Pokemon.setOnClickListener(CapturarActivity.this);
        button_Capturar_Pokemon.setEnabled(false);
        button_Capturar_Pokemon.setText("Wait Loading...");
        imageView_change_ball.setOnClickListener(this);

    }

    private void loadData() {
        //Use RxJava
        Disposable disposable = pokemonRepository.getAllPokemons()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<Pokemon>>() {
                    @Override
                    public void accept(List<Pokemon> pokemons) throws Exception {
                        OnGetAllPokemonSuccess(pokemons);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //Toast.makeText(CapturarActivity.this, "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        compositeDisposable.add(disposable);
    }

    private void OnGetAllPokemonSuccess(List<Pokemon> pokemons) {
        pokemonList.clear();
        pokemonList.addAll(pokemons);
        adapter.notifyDataSetChanged();
    }

    public void mostrarPokemon() {
        textView_pokemon_nome.setText(pokemon.getName());
        textView_pokemon_nivel.setText("Nivel: " + String.valueOf(pokemon.getNivel()));
        Picasso.get().load(pokemon.getImage_url()).into(imageView_pokemon);
    }

    //muda de pokebola
    public void changePokeball() {
        if (ball == 1) {
            imageView_change_ball.setImageDrawable(getDrawable(R.drawable.greatball));
            ball = 2;
        } else if (ball == 2) {
            imageView_change_ball.setImageDrawable(getDrawable(R.drawable.ultraball));
            ball = 3;
        } else if (ball == 3) {
            imageView_change_ball.setImageDrawable(getDrawable(R.drawable.masterball));
            ball = 4;
        } else if (ball == 4) {
            imageView_change_ball.setImageDrawable(getDrawable(R.drawable.pokeball));
            ball = 1;
        } else {
            imageView_change_ball.setImageDrawable(getDrawable(R.drawable.zero));
            ball = 1;
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_Capturar_Pokemon) {
            //Add new pokemon
            Disposable disposable = Observable.create(new ObservableOnSubscribe<Object>() {
                @Override
                public void subscribe(ObservableEmitter<Object> e) throws Exception {
                    pokemonRepository.insertPokemon(pokemon);
                    e.onComplete();
                }
            })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Consumer<Object>() {
                        @Override
                        public void accept(Object o) throws Exception {
                            //Toast.makeText(CapturarActivity.this, "Pokemon added!!!", Toast.LENGTH_SHORT).show();
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Toast.makeText(CapturarActivity.this, "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }, new Action() {
                        @Override
                        public void run() throws Exception {
                            loadData(); //Refresh data
                        }
                    });
            startActivity(new Intent(this, MenuActivity.class));
        }
        if (v.getId() == R.id.imageView_change_ball) {
            changePokeball();
        }
    }
}
