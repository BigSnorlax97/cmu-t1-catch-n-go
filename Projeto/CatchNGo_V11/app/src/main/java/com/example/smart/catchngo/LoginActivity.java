package com.example.smart.catchngo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.squareup.picasso.Picasso;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private Button button_logout, button_entrar;
    private SignInButton signInButton_login;
    private GoogleApiClient googleApiClient;
    private final int REQ_CODE = 9001;
    private TextView textView_nome, textView_email;
    private ImageView imageView_photo;
    private LinearLayout linearLayout_login;
    public static String NOME = " ";
    public static String PHOTO = " ";
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        linearLayout_login = findViewById(R.id.layout_login);
        signInButton_login = findViewById(R.id.signInButton_login);
        button_logout = findViewById(R.id.button_logout);
        textView_nome = findViewById(R.id.textView_nome);
        textView_email = findViewById(R.id.textView_email);
        imageView_photo = findViewById(R.id.imageView_photo);
        button_entrar = findViewById(R.id.button_entrar);

        button_entrar.setEnabled(true);

        signInButton_login.setOnClickListener(this);
        button_logout.setOnClickListener(this);
        button_entrar.setOnClickListener(this);

        GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions).build();

        linearLayout_login.setVisibility(View.GONE);
    }

    private void signIn() {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent, REQ_CODE);
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                updateUI(false);
            }
        });
    }

    private void handleResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            String name = account.getDisplayName();
            String email = account.getEmail();
            String photo_url = " ";
            NOME = name;
            PHOTO = " ";
            textView_nome.setText(name);
            textView_email.setText(email);

            try {
                photo_url = account.getPhotoUrl().toString();
                Picasso.get().load(photo_url).into(imageView_photo);
                PHOTO = photo_url;
            } catch (Exception e) {
                imageView_photo.setImageDrawable(getDrawable(R.drawable.treinador));
            }

            updateUI(true);
        } else {
            updateUI(false);
        }
    }

    private void updateUI(boolean isLogin) {

        if (isLogin) {
            linearLayout_login.setVisibility(View.VISIBLE);
            button_entrar.setEnabled(true);
        } else {
            linearLayout_login.setVisibility(View.GONE);
            button_entrar.setEnabled(false);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.signInButton_login) {
            signIn();
        }
        if (v.getId() == R.id.button_logout) {
            signOut();
        }
        if (v.getId() == R.id.button_entrar) {
            intent = new Intent(this, MenuActivity.class);
            intent.putExtra(NOME, 0);
            intent.putExtra(PHOTO, 0);
            startActivity(intent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleResult(result);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
