package com.example.smart.catchngo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class TreinadorActivity extends AppCompatActivity {

    ImageView imageView_treinador_image;
    TextView textView_nome_treinador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_treinador);

        textView_nome_treinador = findViewById(R.id.textView_nome_treinador);
        imageView_treinador_image = findViewById(R.id.imageView_treinador_image);

        textView_nome_treinador.setText(MenuActivity.NOME_T);
        try {
            Picasso.get().load(MenuActivity.PHOTO_T).into(imageView_treinador_image);
        } catch (Exception e) {
            imageView_treinador_image.setImageDrawable(getDrawable(R.drawable.treinador));
        }
    }
}
