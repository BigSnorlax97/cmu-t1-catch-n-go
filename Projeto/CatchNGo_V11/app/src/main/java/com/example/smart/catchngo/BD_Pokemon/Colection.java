package com.example.smart.catchngo.BD_Pokemon;

public class Colection {

    public class Collection {

        private static final int DEFAULT_SIZE = 1000;
        private Object[] objects;

        public Collection() {
            this.objects = new Object[DEFAULT_SIZE];
        }

        public Object[] getArrayOfObjects() {
            return this.objects;
        }

        public boolean addObject(Object o) {

            int index = 0;
            boolean success = false;

            while (index < objects.length && success == false) {
                if (objects[index] == null) {
                    objects[index] = o;
                    success = true;
                }
                index++;
            }

            return success;
        }

        public Object getObject(int i) {
            if (i >= 0 && i < objects.length) {
                return objects[i];
            }
            return null;
        }

        public void setObject(int i, Object o) {
            if (i >= 0 && i < objects.length) {
                objects[i] = o;
            }
        }

        public int size() {
            return DEFAULT_SIZE;
        }

        public int findObject(Object o) {
            int index = 0;
            boolean found = false;
            while (index < objects.length && found == false) {
                if (o.equals(objects[index])) {
                    found = true;
                } else {
                    index++;
                }
            }
            if (!found) {
                index = -1;
            }
            return index;
        }

    }
}
