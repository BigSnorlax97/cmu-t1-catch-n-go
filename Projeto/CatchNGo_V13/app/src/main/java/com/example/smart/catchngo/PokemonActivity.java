package com.example.smart.catchngo;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smart.catchngo.BD.User;
import com.example.smart.catchngo.BD_Pokemon.Pokemon;
import com.example.smart.catchngo.BD_Pokemon.PokemonDataSource;
import com.example.smart.catchngo.BD_Pokemon.PokemonDatabase;
import com.example.smart.catchngo.BD_Pokemon.PokemonRepository;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class PokemonActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView image;
    private LinearLayout linearLayout_1;
    private TextView textView_espaco_pokemon;
    private ProgressBar progressBar_pokemon;

    //Adapter
    private List<Pokemon> pokemonList = new ArrayList<>();
    private ArrayAdapter adapter;

    //Database
    private CompositeDisposable compositeDisposable;
    private PokemonRepository pokemonRepository;

    private int espaco = 0;

    private ListView listView_pokemons;
    PokemonDatabase pokemonDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon);

        textView_espaco_pokemon = findViewById(R.id.textView_espaco_pokemon);
        progressBar_pokemon = findViewById(R.id.progressbar_pokemon);
        listView_pokemons = findViewById(R.id.listView_pokemons);

        adapter = new ArrayAdapter(PokemonActivity.this, android.R.layout.simple_list_item_1, pokemonList);
        registerForContextMenu(listView_pokemons);
        listView_pokemons.setAdapter(adapter);

        //Init
        compositeDisposable = new CompositeDisposable();

        //Database
        pokemonDatabase = PokemonDatabase.getInstance(this); //create database
        pokemonRepository = PokemonRepository.getInstance(PokemonDataSource.getInstance(pokemonDatabase.pokemonDAO()));

        //Load all data from Database
        loadData();

        textView_espaco_pokemon.setText(espaco + "/400");
        progressBar_pokemon.setProgress(espaco / 4 + espaco % 4);

        Runnable r = new Runnable() {
            @Override
            public void run() {
                textView_espaco_pokemon.setText(espaco + "/400");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    progressBar_pokemon.setProgress(espaco / 4 + espaco % 4, true);
                }
            }
        };
        Handler h = new Handler();
        h.postDelayed(r, 1000);
    }

    private void loadData() {
        Disposable disposable = pokemonRepository.getAllPokemons()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<Pokemon>>() {
                    @Override
                    public void accept(List<Pokemon> pokemons) throws Exception {
                        OnGetAllPokemonSuccess(pokemons);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(PokemonActivity.this, "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        compositeDisposable.add(disposable);
    }

    private void OnGetAllPokemonSuccess(List<Pokemon> pokemons) {
        pokemonList.clear();
        for (int i = 0; i < pokemons.size(); i++) {
            if (!pokemons.get(i).getTreinador().equals(MenuActivity.EMAIL_T)) {
                pokemons.remove(i);
            }
        }
        pokemonList.addAll(pokemons);
        adapter.notifyDataSetChanged();
        espaco = pokemons.size();
    }

    @Override
    public void onClick(View v) {
        textView_espaco_pokemon.setText(espaco + "/400");
        progressBar_pokemon.setProgress(espaco / 4 + espaco % 4);
    }
}
