package com.example.smart.catchngo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smart.catchngo.BD.User;
import com.example.smart.catchngo.BD.UserDataSource;
import com.example.smart.catchngo.BD.UserDatabase;
import com.example.smart.catchngo.BD.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editText_nome;
    private Button button_guardar;
    private TextView textView_nomes;

    private Random rnd = new Random();
    private String nome;

    private ListView listView_users;

    //Adapter
    private List<User> userList = new ArrayList<>();
    private ArrayAdapter adapter;

    //Database
    private CompositeDisposable compositeDisposable;
    private UserRepository userRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        editText_nome = findViewById(R.id.editText_nome);
        button_guardar = findViewById(R.id.button_guardar);
        textView_nomes = findViewById(R.id.textView_nomes);

        button_guardar.setOnClickListener(this);

        //Init
        compositeDisposable = new CompositeDisposable();

        //Init View
        listView_users = findViewById(R.id.listView_users);

        adapter = new ArrayAdapter(SecondActivity.this, android.R.layout.simple_list_item_1, userList);
        registerForContextMenu(listView_users);
        listView_users.setAdapter(adapter);

        //Database
        UserDatabase userDatabase = UserDatabase.getInstance(this); //create database
        userRepository = UserRepository.getInstance(UserDataSource.getInstance(userDatabase.userDAO()));

        //Load all data from Database
        loadData();

    }

    private void loadData() {
        //Use RxJava
        Disposable disposable = userRepository.getAllUsers()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<User>>() {
                    @Override
                    public void accept(List<User> users) throws Exception {
                        OnGetAllUserSuccess(users);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(SecondActivity.this, "1" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        compositeDisposable.add(disposable);
    }

    private void OnGetAllUserSuccess(List<User> users) {
        userList.clear();
        userList.addAll(users);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_guardar) {
            //Add new user
            Disposable disposable = Observable.create(new ObservableOnSubscribe<Object>() {
                @Override
                public void subscribe(ObservableEmitter<Object> e) throws Exception {
                    String nome = editText_nome.getText().toString();
                    if (nome.isEmpty()) {
                        //vazio
                    } else {
                        User user = new User(nome, nome + "@gmail.com");
                        boolean exists = false;
                        for (int i = 0; i < userList.size(); i++) {
                            if (userList.get(i).getEmail().equals(user.getEmail())) {
                                exists = true;
                            }
                        }
                        if(!exists){
                            userRepository.insertUser(user);
                        }
                        e.onComplete();
                    }
                }
            })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Consumer<Object>() {
                        @Override
                        public void accept(Object o) throws Exception {
                            Toast.makeText(SecondActivity.this, "User added!!!", Toast.LENGTH_SHORT).show();
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Toast.makeText(SecondActivity.this, "2" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }, new Action() {
                        @Override
                        public void run() throws Exception {
                            loadData(); //Refresh data
                        }
                    });
        }
    }
}
