package com.example.smart.catchngo;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private SignInButton signInButton_login;
    private GoogleApiClient googleApiClient;
    private final int REQ_CODE = 9001;
    private TextView textView_time;
    public static String NOME;
    public static String EMAIL;
    public static String EXP = " ";
    public static String DATA = " ";
    public static String PHOTO = " ";
    public static String ITEMS = " ";
    public static String ITEMS_TIME = " ";
    private Intent intent;
    private boolean backAllow = false;
    private Runnable runnable;
    private Thread myThread = null;
    private Random rnd = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        signInButton_login = findViewById(R.id.signInButton_login);
        textView_time = findViewById(R.id.textView_time);
        signInButton_login.setOnClickListener(this);

        GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions).build();

        //linearLayout_login.setVisibility(View.INVISIBLE);

        EXP = String.valueOf(rnd.nextInt(20000000));
        DATA = "07/07/2016";
        ITEMS = "100:70:40:10";
        ITEMS_TIME = String.valueOf(Calendar.getInstance().getTimeInMillis() - 300000);

        long timestamp = 0;
        final Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp);
        final int dia = cal.get(Calendar.DAY_OF_MONTH);
        final int mes = cal.get(Calendar.MONTH) + 1;
        final int ano = cal.get(Calendar.YEAR);
        final int hours = cal.get(Calendar.HOUR_OF_DAY);
        final int minutes = cal.get(Calendar.MINUTE);
        final int seconds = cal.get(Calendar.SECOND);

        runnable = new CountDownRunner();
        myThread = new Thread(runnable);
        myThread.start();

        //signOut();

        //signIn();
    }

    private void signIn() {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent, REQ_CODE);
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                //updateUI(false);
            }
        });
    }

    private void handleResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            String name = account.getDisplayName();
            String email = account.getEmail();
            String photo_url = " ";
            NOME = name;
            EMAIL = email;
            PHOTO = " ";

            try {
                photo_url = account.getPhotoUrl().toString();
                PHOTO = photo_url;
            } catch (Exception e) {
            }
entrar();
            //updateUI(true);
        } else {
            //updateUI(false);
        }
    }

    /*
        private void updateUI(boolean isLogin) {

            if (isLogin) {
                linearLayout_login.setVisibility(View.VISIBLE);
                button_entrar.setEnabled(true);
            } else {
                linearLayout_login.setVisibility(View.INVISIBLE);
                button_entrar.setEnabled(false);
            }
        }
    */
    @Override
    public void onBackPressed() {
        if (!backAllow) {
        } else {
            super.onBackPressed();
        }
    }

    class CountDownRunner implements Runnable {
        // @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    doWork();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                } catch (Exception e) {
                }
            }
        }
    }

    public void doWork() {
        runOnUiThread(new Runnable() {
            public void run() {
                try {
                    String horas = String.valueOf(Calendar.getInstance().getTime().getHours());
                    if (Integer.parseInt(horas) < 10) {
                        horas = "0" + horas;
                    }
                    String minutos = String.valueOf(Calendar.getInstance().getTime().getMinutes());
                    if (Integer.parseInt(minutos) < 10) {
                        minutos = "0" + minutos;
                    }
                    String segundos = String.valueOf(Calendar.getInstance().getTime().getSeconds());
                    if (Integer.parseInt(segundos) < 10) {
                        segundos = "0" + segundos;
                    }
                    textView_time.setText(horas + ":" + minutos + ":" + segundos);
                } catch (Exception e) {
                }
            }
        });
    }

    public void entrar(){
        intent = new Intent(this, MenuActivity.class);
        intent.putExtra(NOME, 0);
        intent.putExtra(EMAIL, 0);
        intent.putExtra(EXP, 0);
        intent.putExtra(DATA, 0);
        intent.putExtra(PHOTO, 0);
        intent.putExtra(ITEMS, 0);
        intent.putExtra(ITEMS_TIME, 0);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.signInButton_login) {
            signOut();
            signIn();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleResult(result);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
