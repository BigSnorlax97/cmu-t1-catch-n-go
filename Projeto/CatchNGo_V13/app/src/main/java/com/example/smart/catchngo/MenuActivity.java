package com.example.smart.catchngo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.Calendar;

public class MenuActivity extends AppCompatActivity implements View.OnClickListener {

    private Button button_capturar, button_items, button_treinador, button_pokemon, button_pokedex, button_mapa, button_sair;
    public static String NOME_T = " ";
    public static String EMAIL_T = " ";
    public static String EXP = " ";
    public static String DATA = " ";
    public static String PHOTO_T = " ";
    public static String ITEMS = " ";
    public static String ITEMS_TIME = " ";
    private boolean backAllow = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        button_capturar = findViewById(R.id.button_capturar);
        button_items = findViewById(R.id.button_items);
        button_treinador = findViewById(R.id.button_treinador);
        button_pokemon = findViewById(R.id.button_pokemon);
        button_pokedex = findViewById(R.id.button_pokedex);
        button_mapa = findViewById(R.id.button_mapa);
        button_sair = findViewById(R.id.button_sair);

        button_capturar.setOnClickListener(this);
        button_items.setOnClickListener(this);
        button_treinador.setOnClickListener(this);
        button_pokemon.setOnClickListener(this);
        button_pokedex.setOnClickListener(this);
        button_mapa.setOnClickListener(this);
        button_sair.setOnClickListener(this);

    }
    @Override
    public void onBackPressed() {
        if (!backAllow) {
        } else {
            super.onBackPressed();
        }
    }
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_capturar) {
            EMAIL_T = LoginActivity.EMAIL;
            EXP = LoginActivity.EXP;

            Intent intent = new Intent(this, CapturarActivity.class);
            intent.putExtra(EMAIL_T, 0);
            intent.putExtra(EXP, 0);
            startActivity(intent);
        }
        if (v.getId() == R.id.button_items) {
            ITEMS = LoginActivity.ITEMS;
            ITEMS_TIME = LoginActivity.ITEMS_TIME;
            ITEMS_TIME = String.valueOf(Calendar.getInstance().getTimeInMillis()-300000);

            Intent intent = new Intent(this, ItemsActivity.class);
            intent.putExtra(ITEMS, 0);
            intent.putExtra(ITEMS_TIME, 0);
            startActivity(intent);
        }
        if (v.getId() == R.id.button_treinador) {
            NOME_T = LoginActivity.NOME;
            EMAIL_T = LoginActivity.EMAIL;
            EXP = LoginActivity.EXP;
            DATA = LoginActivity.DATA;
            PHOTO_T = LoginActivity.PHOTO;

            Intent intent = new Intent(this, TreinadorActivity.class);
            intent.putExtra(NOME_T, 0);
            intent.putExtra(EMAIL_T, 0);
            intent.putExtra(EXP, 0);
            intent.putExtra(DATA, 0);
            intent.putExtra(PHOTO_T, 0);
            startActivity(intent);
        }
        if (v.getId() == R.id.button_pokemon) {
            EMAIL_T = LoginActivity.EMAIL;

            Intent intent = new Intent(this, PokemonActivity.class);
            intent.putExtra(EMAIL_T, 0);
            startActivity(intent);
        }
        if (v.getId() == R.id.button_pokedex) {
            startActivity(new Intent(this, PokedexActivity.class));
        }
        if (v.getId() == R.id.button_mapa) {
            startActivity(new Intent(this, SecondActivity.class));
        }
        if (v.getId() == R.id.button_sair) {
            startActivity(new Intent(this, LoginActivity.class));
        }
    }
}
