package com.example.smart.catchngo.BD_Pokemon;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Calendar;

@Entity(tableName = "pokemons")
public class Pokemon {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "numero")
    private int numero;

    @ColumnInfo(name = "nivel")
    private int nivel;

    @ColumnInfo(name = "latitude")
    private String latitude;

    @ColumnInfo(name = "longitude")
    private String longitude;

    @ColumnInfo(name = "image")
    private String image_url;

    @ColumnInfo(name = "localizacao")
    private String localizacao;

    @ColumnInfo(name = "data_captura")
    private long data_captura;

    @ColumnInfo(name = "treinador")
    private String treinador;

    public Pokemon() {
    }

    @Ignore
    public Pokemon(String name, int numero, int nivel) {
        this.name = name;
        this.numero = numero;
        this.nivel = nivel;
        this.image_url = "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/" + numero + ".png";
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getImage_url() {
        return "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/" + numero + ".png";
    }

    public void setImage_url() {
        this.image_url = "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/" + numero + ".png";
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public long getData_captura() {
        return data_captura;
    }

    public void setData_captura(long data_captura) {
        this.data_captura = data_captura;
    }

    private String getDataCapturaString() {
        final long timestamp = data_captura;

        final Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp);

        final int dia = cal.get(Calendar.DAY_OF_MONTH);
        final int mes = cal.get(Calendar.MONTH) + 1;
        final int ano = cal.get(Calendar.YEAR);
        final int hours = cal.get(Calendar.HOUR_OF_DAY);
        final int minutes = cal.get(Calendar.MINUTE);
        final int seconds = cal.get(Calendar.SECOND);

        return (dia + "/" + mes + "/" + ano + "   " + hours + ":" + minutes + ":" + seconds);
        //Toast.makeText(this, "" + pokemon.getData_captura(), Toast.LENGTH_LONG).show();
    }

    public String getTreinador() {
        return treinador;
    }

    public void setTreinador(String treinador) {
        this.treinador = treinador;
    }

    @Override
    public String toString() {
        return new StringBuilder("Treinador: " + treinador +"\nid: " + id + "       Nome: " + name + "\nNº: " + numero + "     Nivel: " + nivel + "\nLatitude: " + latitude +
                "\nLongitude: " + longitude + "\n" + localizacao + "\n" + getDataCapturaString()).toString();
    }
}
