package com.example.smart.catchngo;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class TreinadorActivity extends AppCompatActivity implements View.OnClickListener {

    private ProgressBar progressBar_exp;
    private ImageView imageView_treinador_image;
    private TextView textView_nome_treinador, textView_nivel_treinador, textView_exp_valor, textView_data_inicio;
    private int exp = Integer.parseInt(MenuActivity.EXP);
    private int[] niveis = {-1, 0, 1000, 3000, 6000, 10000, 15000, 21000, 28000, 36000, 45000, 55000, 65000, 75000, 85000, 100000, 120000, 140000, 160000, 185000, 210000, 260000, 335000, 435000, 560000, 710000, 900000, 1100000, 1350000, 1650000, 2000000, 2500000, 3000000, 3750000, 4750000, 6000000, 7500000, 9500000, 12000000, 15000000, 20000000};
    private Button button_ganhar_xp, button_perder_xp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_treinador);

        textView_nome_treinador = findViewById(R.id.textView_nome_treinador);
        textView_data_inicio = findViewById(R.id.textView_data_inicio);
        textView_exp_valor = findViewById(R.id.textView_exp_valor);
        imageView_treinador_image = findViewById(R.id.imageView_treinador_image);
        textView_nivel_treinador = findViewById(R.id.textView_nivel_treinador);
        progressBar_exp = findViewById(R.id.progressBar_exp);
        button_ganhar_xp = findViewById(R.id.button_ganhar_xp);
        button_perder_xp = findViewById(R.id.button_perder_xp);

        calcularNivel(exp, niveis);

        textView_nome_treinador.setText(MenuActivity.NOME_T);
        textView_data_inicio.setText("Data de Inicio:  " + MenuActivity.DATA);

        try {
            Picasso.get().load(MenuActivity.PHOTO_T).into(imageView_treinador_image);
        } catch (Exception e) {
            imageView_treinador_image.setImageDrawable(getDrawable(R.drawable.treinador));
        }

        button_ganhar_xp.setOnClickListener(this);
        button_perder_xp.setOnClickListener(this);

    }

    private void calcularNivel(int exp, int[] niveis) {
        textView_exp_valor.setText("TOTAL XP  " + String.format("%,d", exp));
        if (exp > 20000000) {
            progressBar_exp.setVisibility(View.INVISIBLE);
            textView_nivel_treinador.setText("Nivel  40");
        } else {
            int i;
            for (i = 1; exp >= niveis[i]; i++) {
                textView_nivel_treinador.setText("Nivel  " + i);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                float progresso = (((float) (exp - niveis[i-1])) / ((float) (niveis[i] - niveis[i-1]))) * 100;
                progressBar_exp.setProgress((int) progresso, true);
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_ganhar_xp) {
            exp += 100;
            calcularNivel(exp, niveis);

        }
        if (v.getId() == R.id.button_perder_xp) {
            if (exp - 100 >= 0) {
                exp -= 100;
                calcularNivel(exp, niveis);
            }
        }
    }
}
