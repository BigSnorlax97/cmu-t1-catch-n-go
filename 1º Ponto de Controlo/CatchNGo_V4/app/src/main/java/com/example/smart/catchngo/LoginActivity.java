package com.example.smart.catchngo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    Button button_login_user, button_registar_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        button_login_user = findViewById(R.id.button_login_user);
        button_registar_user = findViewById(R.id.button_registar_user);
        button_login_user.setOnClickListener(this);
        button_registar_user.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.button_login_user){
            startActivity(new Intent(this, MenuActivity.class));
        }
        if(v.getId() == R.id.button_registar_user){
            startActivity(new Intent(this, RegistarActivity.class));
        }
    }
}
