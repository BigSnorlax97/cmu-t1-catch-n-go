package com.example.smart.catchngo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

public class CapturarActivity extends AppCompatActivity implements View.OnClickListener {

    Button button_Capturar_Pokemon;
    ImageView imageView_pokemon, imageView_change_ball;
    TextView textView_pokemon_nome, textView_pokemon_nivel;
    int ball = 1;
    String nome = "Pokemon X";
    Pokemon pokemon = new Pokemon();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capturar);

        button_Capturar_Pokemon = findViewById(R.id.button_Capturar_Pokemon);
        imageView_pokemon = findViewById(R.id.imageView_pokemonImage);
        textView_pokemon_nome = findViewById(R.id.textView_pokemonName);
        textView_pokemon_nivel = findViewById(R.id.textView_Nivel);
        imageView_change_ball = findViewById(R.id.imageView_change_ball);

        button_Capturar_Pokemon.setOnClickListener(this);
        imageView_change_ball.setOnClickListener(this);

        //criar o pokemon a capturar
        Random rnd = new Random();
        int num = rnd.nextInt(802) + 1;
        int nivel = rnd.nextInt(30) + 5;

        pokemon.setNivel(nivel);
        pokemon.setNum(num);

        String url = "https://pokeapi.co/api/v2/pokemon/" + num + "/";
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    nome = response.getString("name");
                    pokemon.setName(nome.substring(0, 1).toUpperCase() + nome.substring(1));
                    mostrarPokemon();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jor);
    }

    public void mostrarPokemon(){
        textView_pokemon_nome.setText(pokemon.getName());
        textView_pokemon_nivel.setText("Nivel: " + String.valueOf(pokemon.getNivel()));
        Picasso.get().load(pokemon.getImage_url()).into(imageView_pokemon);
    }

    //muda de pokebola
    public void changePokeball() {
        if (ball == 1) {
            imageView_change_ball.setImageDrawable(getDrawable(R.drawable.greatball));
            ball = 2;
        } else if (ball == 2) {
            imageView_change_ball.setImageDrawable(getDrawable(R.drawable.ultraball));
            ball = 3;
        } else if (ball == 3) {
            imageView_change_ball.setImageDrawable(getDrawable(R.drawable.masterball));
            ball = 4;
        } else if (ball == 4) {
            imageView_change_ball.setImageDrawable(getDrawable(R.drawable.pokeball));
            ball = 1;
        } else {
            imageView_change_ball.setImageDrawable(getDrawable(R.drawable.zero));
            ball = 1;
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_Capturar_Pokemon) {
            Toast.makeText(this, "Capturado", Toast.LENGTH_LONG).show();
        }
        if (v.getId() == R.id.imageView_change_ball) {
            changePokeball();
        }
    }
}
