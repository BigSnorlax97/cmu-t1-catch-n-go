package com.example.smart.catchngo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Random;

public class PokemonActivity extends AppCompatActivity {

    ImageView image;
    LinearLayout linearLayout_1;
    TextView textView_espaco_pokemon;
    ProgressBar progressBar_pokemon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon);

        textView_espaco_pokemon = findViewById(R.id.textView_espaco_pokemon);
        progressBar_pokemon = findViewById(R.id.progressbar_pokemon);
        int espaco = 0;
        int num;
        linearLayout_1 = findViewById(R.id.linearLayout_1);
        Random rnd = new Random();

        for (int i = 1; i <= 25; i++) {
            LinearLayout linearLayout = new LinearLayout(PokemonActivity.this);
            linearLayout.setGravity(1);
            linearLayout_1.addView(linearLayout);
            for (int j = 1; j <= 5; j++) {
                ImageView image = new ImageView(PokemonActivity.this);
                linearLayout.addView(image);
                num = rnd.nextInt(802) + 1;
                if (num <= 802 && num > 0) {
                    String image_url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + num + ".png";
                    Picasso.get().load(image_url).into(image);
                    espaco++;
                }
            }
        }
        textView_espaco_pokemon.setText(espaco + "/400");
        progressBar_pokemon.setProgress(espaco / 4 + espaco % 4);
    }
}
