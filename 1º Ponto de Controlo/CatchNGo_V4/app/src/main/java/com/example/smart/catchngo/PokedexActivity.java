package com.example.smart.catchngo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

public class PokedexActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView image;
    LinearLayout linearLayout_1;
    public static String NUM = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokedex);

        int num;
        linearLayout_1 = findViewById(R.id.linearLayout_1);
        for (int i = 1; i <= 161; i++) {
            LinearLayout linearLayout = new LinearLayout(PokedexActivity.this);
            linearLayout.setGravity(1);
            linearLayout_1.addView(linearLayout);
            for (int j = 1; j <= 5; j++) {
                ImageView image = new ImageView(PokedexActivity.this);
                num = (((i - 1) * 5) + j);
                if (num <= 802 && num > 0) {
                    image.setId(num);
                    image.setOnClickListener(this);
                    image.setBackground(getDrawable(R.drawable.fundo_2));
                    linearLayout.addView(image);
                    String image_url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + num + ".png";
                    Picasso.get().load(image_url).into(image);
                }
            }
        }
    }

    @SuppressLint("ResourceType")
    @Override
    public void onClick(View v) {
        NUM = String.valueOf(v.getId());
        Intent intent = new Intent(this, PokemonIndividualActivity.class);
        intent.putExtra(NUM, 0);
        startActivity(intent);
    }
}
