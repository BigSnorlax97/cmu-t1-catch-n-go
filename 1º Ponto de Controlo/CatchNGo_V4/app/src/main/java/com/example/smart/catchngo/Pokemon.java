package com.example.smart.catchngo;

public class Pokemon {

    private String name;
    private int num;
    private int nivel;
    private String image_url;

    Pokemon(){}

    Pokemon(String name, int num, int nivel){
        this.name = name;
        this.num = num;
        this.nivel=nivel;
        this.image_url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + num + ".png";
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public int getNum(){
        return num;
    }

    public void setNum(int num){
        this.num = num;
        this.image_url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + num + ".png";
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
