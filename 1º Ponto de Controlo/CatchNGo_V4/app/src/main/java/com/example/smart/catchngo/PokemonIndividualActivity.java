package com.example.smart.catchngo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class PokemonIndividualActivity extends AppCompatActivity implements View.OnClickListener {

    TextView textView_pokemon_numero_nome, textView_descricao;
    ImageView imageView_pokemon_image, imageView_anterior, imageView_atual, imageView_seguinte;
    String nome, image_url;
    int numero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_individual);

        textView_pokemon_numero_nome = findViewById(R.id.textView_pokemon_numero_nome);
        imageView_pokemon_image = findViewById(R.id.imageView_pokemon_image);
        textView_descricao = findViewById(R.id.textView_pokemon_descricao);
        imageView_anterior = findViewById(R.id.imageView_anterior);
        imageView_atual = findViewById(R.id.imageView_atual);
        imageView_seguinte = findViewById(R.id.imageView_seguinte);

        numero = Integer.parseInt(PokedexActivity.NUM);

        getBeforeAfterPokemon();
        getPokemonDex();

        imageView_anterior.setOnClickListener(this);
        imageView_seguinte.setOnClickListener(this);
    }

    public void getBeforeAfterPokemon(){
        if(numero >=1){
            image_url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + (numero - 1) + ".png";
            Picasso.get().load(image_url).into(imageView_anterior);
        } else {
            imageView_pokemon_image.setImageDrawable(getDrawable(R.drawable.zero));
        }

        image_url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + numero + ".png";
        Picasso.get().load(image_url).into(imageView_atual);

        if(numero <=802){
            image_url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + (numero + 1) + ".png";
            Picasso.get().load(image_url).into(imageView_seguinte);
        } else {
            imageView_pokemon_image.setImageDrawable(getDrawable(R.drawable.zero));
        }
    }

    public void getPokemonDex() {
        image_url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + numero + ".png";
        Picasso.get().load(image_url).into(imageView_pokemon_image);

        String url = "https://pokeapi.co/api/v2/pokemon/" + numero + "/";
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    nome = response.getString("name");
                    if(numero<10)
                    textView_pokemon_numero_nome.setText("No. 00" + numero);
                    else if(numero<100)
                        textView_pokemon_numero_nome.setText("No. 0" + numero);
                    else
                        textView_pokemon_numero_nome.setText("No. " + numero);
                    textView_pokemon_numero_nome.append("   " + nome.substring(0, 1).toUpperCase() + nome.substring(1));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jor);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imageView_anterior) {
            if (numero > 1) {
                numero--;
                textView_pokemon_numero_nome.setText("");
                getPokemonDex();
                getBeforeAfterPokemon();
            }
        }
        if (v.getId() == R.id.imageView_seguinte) {
            if (numero < 802) {
                numero++;
                textView_pokemon_numero_nome.setText("");
                getPokemonDex();
                getBeforeAfterPokemon();
            }
        }
    }
}
