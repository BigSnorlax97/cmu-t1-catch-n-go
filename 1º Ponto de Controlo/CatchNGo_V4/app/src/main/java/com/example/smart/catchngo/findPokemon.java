package com.example.smart.catchngo;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

public class findPokemon extends AppCompatActivity implements View.OnClickListener {

    private TextView textView_pokemonName;
    private ImageView imageView_pokemonImage;
    private TextView textView_Latitude;
    private TextView textView_Longitude;

    private LocationManager locationManager;
    private LocationListener locationListener;

    private String locationProvider = LocationManager.NETWORK_PROVIDER;

    private int num = 0;
    String nome, url, urlEvolution, evolution;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_pokemon);

        num = Integer.parseInt(MainActivity.NUMERO);

        textView_pokemonName = findViewById(R.id.textView_pokemonName);
        imageView_pokemonImage = findViewById(R.id.imageView_pokemonImage);
        textView_Latitude = findViewById(R.id.textView_Latitude);
        textView_Longitude = findViewById(R.id.textView_Longitude);

        getPokemonName();
        getPokemonImage();
        getEvolutionURL();
        //getEvolution();

        location();

    }

    public void location() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                textView_Latitude.setText("Latitude: " + String.valueOf(location.getLatitude()));
                textView_Longitude.setText("Longitude: " + String.valueOf(location.getLongitude()));
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET
                }, 10);
                return;
            }
        } else {
            configureButton();
        }
        configureButton();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 10:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    configureButton();
                return;
        }
    }

    private void configureButton() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 0, locationListener);
    }

    public void getEvolutionURL() {
        url = "https://pokeapi.co/api/v2/pokemon-species/" + num + "/";
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject chain = response.getJSONObject("evolution_chain");
                    urlEvolution = chain.getString("url");
                    System.out.println(urlEvolution);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jor);
    }

    public void getEvolution() {
        if (num > 0) {
            JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, evolution, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        System.out.println(evolution);
                        JSONObject chain = response.getJSONObject("chain");
                        JSONArray evolves_to = chain.getJSONArray("evolves_to");
                        JSONObject species = evolves_to.getJSONObject(1);
                        evolution = species.getString("name");
                        textView_pokemonName.append("\n" + evolution);
                        System.out.println("------------------" + evolution);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            RequestQueue queue = Volley.newRequestQueue(this);
            queue.add(jor);
        } else {
            textView_pokemonName.setText("Valor inválido");
        }
    }

    public void getPokemonName() {
        if (num > 0) {
            url = "https://pokeapi.co/api/v2/pokemon/" + num + "/";
            JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        nome = response.getString("name");
                        textView_pokemonName.setText(nome.substring(0, 1).toUpperCase() + nome.substring(1));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            RequestQueue queue = Volley.newRequestQueue(this);
            queue.add(jor);
        } else {
            textView_pokemonName.setText("Valor inválido");
        }
    }

    public void getPokemonImage() {
        final String imageUrl = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + num + ".png";

        Picasso.get().load(imageUrl).into(imageView_pokemonImage);
    }

    @Override
    public void onClick(View v) {
        System.out.println("muda crl");
        Random rnd = new Random();
        findViewById(R.id.find_layout_x).setBackgroundColor(Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)));

    }
}