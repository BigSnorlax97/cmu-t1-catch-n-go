package com.example.smart.catchngo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editText_pokemonNumber;
    private Button button_play;
    private TextView textView_invalido;

    public static String NUMERO;

    private int num;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText_pokemonNumber = findViewById(R.id.editText_pokemonNumber);
        button_play = findViewById(R.id.button_play);
        textView_invalido = findViewById(R.id.textView_invalido);

        this.button_play.setOnClickListener(this);
        this.editText_pokemonNumber.setOnClickListener(this);

    }

    public void findActivity(){
        try {
            num = Integer.parseInt(editText_pokemonNumber.getText().toString());
        } catch (Exception e) {
            num = 0;
        }

        if (num < 1 || num > 802)
            num = 0;

        NUMERO = String.valueOf(num);
        if (num != 0) {
            Intent intent = new Intent(this, findPokemon.class);
            intent.putExtra(NUMERO, 0);
            this.startActivity(intent);
        } else{
            textView_invalido.setText("Valor Inválido!");
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_play) {
            findActivity();
        }
        if (v.getId() == R.id.editText_pokemonNumber) {
            editText_pokemonNumber.setText("");
        }
    }
}