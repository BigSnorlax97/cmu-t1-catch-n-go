package com.example.smart.catchngo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MenuActivity extends AppCompatActivity implements View.OnClickListener {

    Button button_capturar, button_items, button_treinador, button_pokemon, button_pokedex, button_mapa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        button_capturar = findViewById(R.id.button_capturar);
        button_items = findViewById(R.id.button_items);
        button_treinador = findViewById(R.id.button_treinador);
        button_pokemon = findViewById(R.id.button_pokemon);
        button_pokedex = findViewById(R.id.button_pokedex);
        button_mapa = findViewById(R.id.button_mapa);

        button_capturar.setOnClickListener(this);
        button_items.setOnClickListener(this);
        button_treinador.setOnClickListener(this);
        button_pokemon.setOnClickListener(this);
        button_pokedex.setOnClickListener(this);
        button_mapa.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.button_capturar){
            startActivity(new Intent(this, CapturarActivity.class));
        }
        if(v.getId() == R.id.button_items){
            startActivity(new Intent(this, ItemsActivity.class));
        }
        if(v.getId() == R.id.button_treinador){
            startActivity(new Intent(this, TreinadorActivity.class));
        }
        if(v.getId() == R.id.button_pokemon){
            startActivity(new Intent(this, PokemonActivity.class));
        }
        if(v.getId() == R.id.button_pokedex){
            startActivity(new Intent(this, PokedexActivity.class));
        }
        if(v.getId() == R.id.button_mapa){
            startActivity(new Intent(this, MapaActivity.class));
        }
    }
}
