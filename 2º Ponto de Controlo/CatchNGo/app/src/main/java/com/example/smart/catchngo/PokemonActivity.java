package com.example.smart.catchngo;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smart.catchngo.BD_Pokemon.Pokemon;
import com.example.smart.catchngo.BD_Pokemon.PokemonDataSource;
import com.example.smart.catchngo.BD_Pokemon.PokemonDatabase;
import com.example.smart.catchngo.BD_Pokemon.PokemonRepository;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class PokemonActivity extends AppCompatActivity {

    private TextView textView_espaco_pokemon;
    private ProgressBar progressBar_pokemon;

    //Adapter
    private List<Pokemon> pokemonList = new ArrayList<>();
    private ArrayAdapter adapter;

    //Database
    private CompositeDisposable compositeDisposable;
    private PokemonRepository pokemonRepository;

    private int espaco = 0;

    private ListView listView_pokemons;
    PokemonDatabase pokemonDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon);

        textView_espaco_pokemon = findViewById(R.id.textView_espaco_pokemon);
        progressBar_pokemon = findViewById(R.id.progressbar_pokemon);
        listView_pokemons = findViewById(R.id.listView_pokemons);

        adapter = new ArrayAdapter(PokemonActivity.this, android.R.layout.simple_list_item_1, pokemonList);
        registerForContextMenu(listView_pokemons);
        listView_pokemons.setAdapter(adapter);

        //Init
        compositeDisposable = new CompositeDisposable();

        //Database
        pokemonDatabase = PokemonDatabase.getInstance(this); //create database
        pokemonRepository = PokemonRepository.getInstance(PokemonDataSource.getInstance(pokemonDatabase.pokemonDAO()));

        //Load all data from Database
        loadData();

        Runnable r = new Runnable() {
            @Override
            public void run() {
                textView_espaco_pokemon.setText(espaco + "/400");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    progressBar_pokemon.setProgress(espaco / 4 + espaco % 4, true);
                } else {
                    progressBar_pokemon.setProgress(espaco / 4 + espaco % 4);
                }
            }
        };
        Handler h = new Handler();
        h.postDelayed(r, 1000); //espera 1 segundo
    }

    /**
     * carrega os dados da base de dados
     */
    private void loadData() {
        Disposable disposable = pokemonRepository.getAllPokemons()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<Pokemon>>() {
                    @Override
                    public void accept(List<Pokemon> pokemons) throws Exception {
                        OnGetAllPokemonSuccess(pokemons);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(PokemonActivity.this, "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        compositeDisposable.add(disposable);
    }

    /**
     * Tendo em conta o email da conta a ser usada
     * Adiciona à listView apenas os Pokemons que foram capturados pelo utilizador atual
     *
     * @param pokemons - todos os Pokemons da base de dados
     */
    private void OnGetAllPokemonSuccess(List<Pokemon> pokemons) {
        pokemonList.clear();
        int tam = pokemons.size();
        List<Pokemon> poke = new ArrayList<>();
        for (int i = 0; i < tam; i++) {
            if (pokemons.get(i).getTreinador().equals(LoginActivity.EMAIL)) {
                poke.add(pokemons.get(i));
            }
        }
        pokemonList.addAll(poke);
        adapter.notifyDataSetChanged();
        espaco = poke.size();
    }
}
