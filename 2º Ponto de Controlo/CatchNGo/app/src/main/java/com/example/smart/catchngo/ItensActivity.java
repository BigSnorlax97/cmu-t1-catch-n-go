package com.example.smart.catchngo;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smart.catchngo.BD.User;
import com.example.smart.catchngo.BD.UserDataSource;
import com.example.smart.catchngo.BD.UserDatabase;
import com.example.smart.catchngo.BD.UserRepository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ItensActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView textView_pokeball, textView_greatball, textView_ultraball, textView_masterball;
    private Button button_receber_items;
    private int pokeballs = 0, greatballs = 0, ultraballs = 0, masterballs = 0;
    private String[] items = new String[4];
    private long itens_time = 0;
    private User user;

    //Thread
    private Runnable runnable;
    private Thread myThread = null;

    //Adapter
    private List<User> userList = new ArrayList<>();

    //Database
    private CompositeDisposable compositeDisposable;
    private UserRepository userRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_itens);

        textView_pokeball = findViewById(R.id.item_pokeball);
        textView_greatball = findViewById(R.id.item_greatball);
        textView_ultraball = findViewById(R.id.item_ultraball);
        textView_masterball = findViewById(R.id.item_masterball);
        button_receber_items = findViewById(R.id.button_recebe_itens);

        textView_pokeball.setText("Pokeball X " + pokeballs);
        textView_greatball.setText("Greatball X " + greatballs);
        textView_ultraball.setText("Ultraball X " + ultraballs);
        textView_masterball.setText("Masterball X " + masterballs);

        button_receber_items.setVisibility(View.INVISIBLE);
        button_receber_items.setOnClickListener(this);

        //Init
        compositeDisposable = new CompositeDisposable();

        //Database
        UserDatabase userDatabase = UserDatabase.getInstance(this); //create database
        userRepository = UserRepository.getInstance(UserDataSource.getInstance(userDatabase.userDAO()));
        loadData();

        Runnable r = new Runnable() {
            @Override
            public void run() {
                findUser(); //encontrar o User
                carregarItens(); //vai buscar os itens do User

                runnable = new CountDownRunner();
                myThread = new Thread(runnable);
                myThread.start();

                button_receber_items.setVisibility(View.VISIBLE);
            }
        };
        Handler h = new Handler();
        h.postDelayed(r, 1000);

    }

    public void carregarItens() {
        String itens = user.getItens();

        pokeballs = Integer.parseInt(itens.split(":")[0]);
        greatballs = Integer.parseInt(itens.split(":")[1]);
        ultraballs = Integer.parseInt(itens.split(":")[2]);
        masterballs = Integer.parseInt(itens.split(":")[3]);

        textView_pokeball.setText("Pokeball X " + pokeballs);
        textView_greatball.setText("Greatball X " + greatballs);
        textView_ultraball.setText("Ultraball X " + ultraballs);
        textView_masterball.setText("Masterball X " + masterballs);

        itens_time = user.getItens_time(); //data da ultima vez que recebeu itens
    }

    private void loadData() {
        Disposable disposable = userRepository.getAllUsers()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<User>>() {
                    @Override
                    public void accept(List<User> users) throws Exception {
                        onGetAllUserSuccess(users);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(ItensActivity.this, "fail\n" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        compositeDisposable.add(disposable);
    }

    private void onGetAllUserSuccess(List<User> users) {
        userList.clear();
        userList.addAll(users);
    }

    /**
     * Procura na base de dados o User com o email da conta google que usou para entrar no jogo
     * e atribui esse User ao objeto User para ter acesso aos dados mais facilmente
     */
    public void findUser() {
        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).getEmail().equals(LoginActivity.EMAIL)) {
                user = userList.get(i);
            }
        }
    }

    /**
     * Atualiza os dados do utilizador
     *
     * @param user - objeto do tipo User
     */
    public void updateUser(final User user) {
        Disposable disposable = Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> e) throws Exception {
                userRepository.updateUser(user);
                e.onComplete();
            }
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object o) throws Exception {

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(ItensActivity.this, "2" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, new Action() {
                    @Override
                    public void run() throws Exception {
                        loadData(); //Refresh data
                    }
                });
        compositeDisposable.add(disposable);
    }


    /**
     * Adiciona Pokebolas
     */
    public void adicionarPokeBolas() {
        Random rnd = new Random();
        pokeballs += rnd.nextInt(5) + 2;
        textView_pokeball.setText("Pokeball X " + pokeballs);
        greatballs += rnd.nextInt(3) + 2;
        textView_greatball.setText("Greatball X " + greatballs);
        ultraballs += rnd.nextInt(2) + 1;
        textView_ultraball.setText("Ultraball X " + ultraballs);
        masterballs += rnd.nextInt(2);
        textView_masterball.setText("Masterball X " + masterballs);
    }

    /**
     * Gera uma notificação programada para os próximos 5 minutos, para avisar o utilizador que pode ir receber mais itens
     */
    public void notification() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        Intent notificationIntent = new Intent(this, AlarmReceiver.class);
        PendingIntent broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 300);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);
    }

    class CountDownRunner implements Runnable {
        // @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    doWork(); //verifica se ja passaram 5 minutos desda ultima vez que clicou em receber itens
                    Thread.sleep(100); //intervalo de 100 milissegundos = 0.1 segundos
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                } catch (Exception e) {
                }
            }
        }
    }

    /**
     * Função chamada pela Thread
     * Verifica se ja passaram 5 minutos desde as ultima vez que recebeu itens
     */
    public void doWork() {
        runOnUiThread(new Runnable() {
            public void run() {
                try {
                    if (Calendar.getInstance().getTimeInMillis() > itens_time + 300000) { // 300 000 milissegundo = 5 minutos
                        button_receber_items.setEnabled(true);
                        button_receber_items.setText(" Receber Itens ");
                    } else {
                        button_receber_items.setEnabled(false);
                        button_receber_items.setText(" Pode receber mais itens daqui a 5 minutos ");
                    }
                } catch (Exception e) {
                }
            }
        });
    }

    /**
     * Interrompe a Threaad que está a correr, quando saimos desta atividade
     */
    @Override
    protected void onStop() {
        myThread.interrupt();
        super.onStop();
    }

    /**
     * Guarda as quantidades de itens e a ultima vez que clicou no botao para receber itens no objeto
     * User e chama a funcão updateUser, para atualizar os dados do utilizador na base de dados
     */
    public void guardarItens() {
        String itens = pokeballs + ":" + greatballs + ":" + ultraballs + ":" + masterballs;
        user.setItens(itens);
        user.setItens_time(itens_time);
        updateUser(user);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_recebe_itens) { //clicar no botao de receber itens
            adicionarPokeBolas(); //adicionar pokebolas
            notification(); //preparar a notificaçao
            itens_time = Calendar.getInstance().getTimeInMillis(); //guardar o momento em que clicou no botao
            guardarItens(); //guarda itens na base de dados
            //nao deixa o utilizador clicar, a Thread verifica contantemente se ja passaram 5 minutos
            button_receber_items.setEnabled(false);
            button_receber_items.setText(" Pode receber mais itens daqui a 5 minutos ");
        }
    }
}
